//
//  csgPolygon.hpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgPolygon_hpp
#define csgPolygon_hpp

#include <vector>
#include "csgVertex.hpp"
#include "csgPlane.hpp"

class CSGPolygon {
public:
    std::vector<CSGVertex> vertices;
    CSGPlane plane;
    
    CSGPolygon(std::vector<CSGVertex> vertices);
    CSGPolygon clone();
    void flip();
};

#endif /* csgPolygon_hpp */
