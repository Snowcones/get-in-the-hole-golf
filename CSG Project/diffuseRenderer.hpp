//
//  diffuseRenderer.hpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef diffuseRenderer_hpp
#define diffuseRenderer_hpp

//Stuff needed
#include <stdio.h>
#include "csg.hpp"
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include "openGLUtil.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

class diffuseRenderer
{
protected:
    void setupBuffers();
    static void loadAndLinkShaders();
    void updateViewingMatrices();

    glm::vec2 orientation;
    glm::vec3 translation;
    glm::vec4 color;
    static bool isLoaded;

public:
    diffuseRenderer();
    void bufferTriList(int numTriangles, float* triangleData);
    void render();

    void setRenderColor(glm::vec4 c);
    void setOrientation(glm::vec2 o);
    void setTranslation(glm::vec3 t);
    void rotate(glm::vec2 r);
    void translate(glm::vec3 t);

    glm::vec2 getOrientation(); //This data shouldn't need to be read
    glm::vec3 getTranslation();

    int verticesToDraw;
    GLuint vao;
    GLuint vbo;
    static GLuint posAttrib;
    static GLuint normAttrib;
    static GLuint prog;    //Could be static

    static GLuint colorUniform;
    static GLuint modelMatUniform;
    static GLuint viewMatUniform;
    static GLuint projMatUniform;

    glm::mat4 modelMat;
    glm::mat4 viewMat;
    glm::mat4 projMat;

    static int screenWidth;
    static int screenHeight;
};

#endif /* diffuseRenderer_hpp */
