#version 330

in vec3 pos;
in vec3 norm;

uniform vec4 color;
uniform mat4 modelMat;
uniform mat4 projMat;
uniform mat4 viewMat;

out vec4 drawColor;

void main()
{
    gl_Position = projMat * viewMat * modelMat * vec4(pos, 1.0);
    vec3 newNorm = (viewMat * modelMat * vec4(norm, 0.0)).xyz;
    drawColor = vec4(color.rgb * min(dot(newNorm, vec3(1.0, 1.0, 1.0)), .2), color.a);
}