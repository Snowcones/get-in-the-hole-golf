//
//  bspGeometry.hpp
//  CSG Project
//
//  Created by William Henning on 5/12/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef bspGeometry_hpp
#define bspGeometry_hpp

#include <stdio.h>
#include "csg.hpp"


bspTreeNode* treeFromTriList(std::vector<triangle> tris);

bspTreeNode* bspCube(glm::vec3 scale = glm::vec3(1), glm::vec3 rot = glm::vec3(0), glm::vec3 center = glm::vec3(0));
bspTreeNode* bspSphere(float radius);

bspTreeNode* bspSphere(float radius = 1.0, glm::vec3 center = glm::vec3(0), int thetaRes = 20, int phiRes = 10);
bspTreeNode* bspSpheroid(glm::vec3 scale = glm::vec3(1), glm::vec3 rot = glm::vec3(0), glm::vec3 center = glm::vec3(0), int thetaRes = 20, int phiRes = 10);

#endif /* bspGeometry_hpp */
