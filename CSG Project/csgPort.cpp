//
//  csgPort.cpp
//  CSG Project
//
//  Created by William Henning on 6/12/16.
//  Copyright © 2016 William Henning. All rights reserved.
//
//  This code is my port of csg.js http://evanw.github.io/csg.js/docs/ to c++
//

#include "csgPort.hpp"


//I'm ommiting the shared member from the CSGPolygon class for now at least. I don't have a need for it and am not sure how to implement it.
//Otherwise this code is a fairly faithful port of csg.js.
//I don't think the calls to .clone() in my code are necessary.



//The defaut constructor is sufficent
CSG::CSG() {
    
}

CSG::CSG(std::vector<CSGPolygon> inPolygons) {
    polygons = inPolygons;
}

CSG CSG::fromPolygons(std::vector<CSGPolygon> inPolygons) {
    CSG csg = CSG();
    csg.polygons = inPolygons;
    return csg;
}

CSG CSG::clone() {
    CSG csg = CSG();
    for (CSGPolygon polygon : polygons)
    {
        csg.polygons.push_back(polygon.clone());
    }
    return csg;
}

std::vector<CSGPolygon> CSG::toPolygons() {
    return this->polygons;
}

CSG CSG::unionOf(CSG csg) {
    if (this->polygons.size() == 0) return csg;
    if (csg.polygons.size() == 0)   return *this;
    
    CSGNode a = CSGNode(this->clone().polygons);
    CSGNode b = CSGNode(csg.clone().polygons);

    a.clipTo(&b);
    b.clipTo(&a);
    b.invert();
    b.clipTo(&a);
    b.invert();
    a.build(b.allPolygons());
    return CSG::fromPolygons(a.allPolygons());
}

CSG CSG::subtract(CSG csg) {
    if (this->polygons.size() == 0) return *this;
    if (csg.polygons.size() == 0)   return *this;
    
    CSGNode a = CSGNode(this->clone().polygons);
    CSGNode b = CSGNode(csg.clone().polygons);

    a.invert();
    a.clipTo(&b);
    b.clipTo(&a);
    b.invert();
    b.clipTo(&a);
    b.invert();
    a.build(b.allPolygons());
    a.invert();
    return CSG::fromPolygons(a.allPolygons());
}

CSG CSG::intersect(CSG csg) {
    if (this->polygons.size() == 0) return *this;
    if (csg.polygons.size() == 0)   return csg;
    
    CSGNode a = CSGNode(this->clone().polygons);
    CSGNode b = CSGNode(csg.clone().polygons);
    
    a.invert();
    b.invert();
    a.clipTo(&b);
    b.clipTo(&a);
    a.build(b.allPolygons());
    a.invert();
    return CSG::fromPolygons(a.allPolygons());
}

CSG CSG::inverse() {
    CSG csg = this->clone();
    for ( CSGPolygon& poly  : csg.polygons )
    {
        poly.flip();
    }
    return csg;
}