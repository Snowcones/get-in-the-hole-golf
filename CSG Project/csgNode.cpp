//
//  csgNode.cpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "csgNode.hpp"
#include "csgPolygon.hpp"
#include <algorithm>

CSGNode::CSGNode() {
    plane = NULL;
    front = NULL;
    back = NULL;
}

CSGNode::CSGNode(std::vector<CSGPolygon> inPolygons) {
//    printf("Called node constructor with :%lu polygons\n", inPolygons.size());
    plane = NULL;
    front = NULL;
    back = NULL;
    this->build(inPolygons);
}

CSGNode::~CSGNode() {
    if (front)
    {
        delete front;
        front = NULL;
    }
    if (back)
    {
        delete back;
        back = NULL;
    }
    if (plane)
    {
        delete plane;
        plane = NULL;
    }
}

CSGNode::CSGNode(const CSGNode& other) {
    //printf("Copy called\n");
    plane = NULL;
    front = NULL;
    back  = NULL;
    if (other.plane) plane = other.plane->duplicate();
    if (other.front) front = other.front->clone();
    if (other.back)  back  = other.back->clone();
    for (CSGPolygon polygon : other.polygons)
    {
        polygons.push_back(polygon.clone());
    }
}

CSGNode& CSGNode::operator=(CSGNode other) {
    assert(0);
}

CSGNode* CSGNode::clone() {
    CSGNode* node = new CSGNode();
    if (plane) node->plane = plane->duplicate();
    if (front) node->front = front->clone();
    if (back)  node->back  = back->clone();
    for (CSGPolygon polygon : polygons)
    {
        node->polygons.push_back(polygon.clone());
    }
    return node;
}

void CSGNode::invert() {
    for (int i = 0; i < polygons.size(); i++) {
        polygons[i].flip();
    }
    if (plane) plane->flip();
    if (front) front->invert();
    if (back) back->invert();
    std::swap(front, back);
}

std::vector<CSGPolygon> CSGNode::clipPolygons(std::vector<CSGPolygon> clipPolygons) {
    //if (!plane) return polygons; //Calls a copy with slice() in csg.js, not sure if necessary
    if (!plane)
    {
        std::vector<CSGPolygon> out = clipPolygons;
        return out;
    }
    std::vector<CSGPolygon> f;
    std::vector<CSGPolygon> b;
    for (int i = 0; i < clipPolygons.size(); i++)
    {
        plane->splitPolygon(clipPolygons[i], f, b, f, b);
    }
    if (front) f = front->clipPolygons(f);
    if (back)  b = back->clipPolygons(b);
    else b.clear();
    f.insert(std::end(f), std::begin(b), std::end(b));
    return f;
}

void CSGNode::clipTo(CSGNode* bsp) {
    polygons = bsp->clipPolygons(polygons);
    if (front) front->clipTo(bsp);
    if (back) back->clipTo(bsp);
}

void CSGNode::testClipTo(CSGNode bsp, std::vector<float>& data) {
    float before = polygons.size();
    polygons = bsp.clipPolygons(polygons);
    float end = polygons.size();
    data.push_back(end/before);
    if (front) front->testClipTo(bsp, data);
    if (back) back->testClipTo(bsp, data);
}

std::vector<CSGPolygon> CSGNode::allPolygons() {
    std::vector<CSGPolygon> outPolygons = polygons;
    if (front) {
        std::vector<CSGPolygon> append = front->allPolygons();
        outPolygons.insert(std::end(outPolygons), std::begin(append), std::end(append));
    }
    if (back) {
        std::vector<CSGPolygon> append = back->allPolygons();
        outPolygons.insert(std::end(outPolygons), std::begin(append), std::end(append));
    }
    return outPolygons;
}

void CSGNode::build(std::vector<CSGPolygon> buildPolygons) {
    if (buildPolygons.size() == 0) return;
    if (!plane)
    {
        plane = buildPolygons[0].plane.duplicate();
        
        polygons.push_back(buildPolygons[0]);
        std::swap(buildPolygons.front(), buildPolygons.back());
        buildPolygons.pop_back();
    }
    std::vector<CSGPolygon> f;
    std::vector<CSGPolygon> b;
    for (int i = 0; i < buildPolygons.size(); i++)
    {
        plane->splitPolygon(buildPolygons[i], polygons, polygons, f, b);
    }
    if (f.size() > 0)
    {
        if (!front) front = new CSGNode();
        front->build(f);
    }
    if (b.size() > 0)
    {
        if (!back) back = new CSGNode();
        back->build(b);
    }
}