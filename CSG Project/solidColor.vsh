#version 330

in vec4 vertData;

uniform vec4 inColor;
uniform vec4 outColor;

uniform mat4 modelMat;
uniform mat4 projMat;
uniform mat4 viewMat;

out vec4 drawColor;

void main()
{
    gl_Position = projMat * viewMat * modelMat * vec4(vertData.xyz, 1.0);
    drawColor = inColor*vertData.w + outColor*(1-vertData.w);
    if (drawColor.a < .01)
    {
        gl_Position = vec4(-1);
    }
}