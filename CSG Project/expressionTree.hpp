//
//  expressionTree.hpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef expressionTree_hpp
#define expressionTree_hpp

#include "csgPort.hpp"
#include <glm/glm.hpp>
#include "csgTesting.hpp"

typedef enum : int {
    K_INVERT,
    K_SUBTRACT,
    K_OR,
    K_AND
} CSGOperation;

class ExpressionNode {
private:

    
    
    
    bool testPointLeft(glm::vec3 p);
    bool testPointRight(glm::vec3 p);
    
public:
    CSG leftSide();
    CSG rightSide();
    
    CSGOperation op;
    
    ExpressionNode* leftExpression;
    ExpressionNode* rightExpression;
    
    CSGNode* leftShape;
    CSGNode* rightShape;
    
    ExpressionNode(CSGOperation inOp, CSG* inLeft, CSG* inRight);
    ExpressionNode(CSGOperation inOp, ExpressionNode* inLeft, ExpressionNode* inRight);
    
    ~ExpressionNode();
    
    
    CSG getShape();
    bool testPoint(glm::vec3 p);
//    bool testPointFromConstructed(glm::vec3 p);
};

//Generates a csg expression tree randomly with n layers and [1, 2^n] (avr (7/4)^n) shapes depending on inverse calls
ExpressionNode* randomExpressionTree(int n);

CSG* randomShape();

#endif /* expressionTree_hpp */
