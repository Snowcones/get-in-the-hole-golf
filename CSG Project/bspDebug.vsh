#version 330

in vec4 vertData;

uniform vec4 frontColor;
uniform vec4 thisColor;
uniform vec4 backColor;

uniform mat4 modelMat;
uniform mat4 projMat;
uniform mat4 viewMat;

out vec4 drawColor;

void main()
{
    vec4 drawPos = projMat * viewMat * modelMat * vec4(vertData.xyz, 1.0);
    
    drawColor = thisColor;
    if (vertData.w < .5)
    {
        drawColor = frontColor;
        drawPos += vec4(0, 0, .0005, 0);    //Draws front faces behind current face
    }
    if (vertData.w > 1.5)
    {
        drawColor = backColor;
        drawPos += vec4(0, 0, .001, 0);     //Draws back faces behind current face and front faces
    }
    
    gl_Position = drawPos;
}