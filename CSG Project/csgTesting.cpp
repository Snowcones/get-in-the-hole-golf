//
//  csgTesting.cpp
//  CSG Project
//
//  Created by William Henning on 5/15/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "csgTesting.hpp"

bool pointIsInBSPTree(glm::vec3 p, bspTreeNode* n) {
    if (n->triNode.n == glm::vec3(0))
    {
        return false;
    }
    float d = n->dist(p);
    if (d <= 0)
    {
        if (n->backSubtree == NULL) return true;
        else return pointIsInBSPTree(p, n->backSubtree);
    }
    if (d > 0)
    {
        if (n->frontSubtree == NULL) return false;
        else return pointIsInBSPTree(p, n->frontSubtree);
    }
    return false;
}

bool pointIsInBSPTree(glm::vec3 point, const CSGNode& t) {
    if (!t.plane) return false;
        
    float dist = glm::dot(t.plane->normal, point) - t.plane->w;
    if (dist <= 0)
    {
        if (!t.back) return true;
        else return pointIsInBSPTree(point, *t.back);
    }
    if (dist > 0)
    {
        if (!t.front) return false;
        else return pointIsInBSPTree(point, *t.front);
    }
    return false;
}