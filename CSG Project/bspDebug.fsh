#version 330

in vec4 drawColor;
out vec4 fragColor;

void main()
{
    fragColor = drawColor;
}