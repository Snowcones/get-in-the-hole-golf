//
//  csgNode.hpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgNode_hpp
#define csgNode_hpp

#include <vector>

class CSGPlane;
class CSGPolygon;

class CSGNode {
public:
    CSGPlane* plane;
    CSGNode* front;
    CSGNode* back;
    std::vector<CSGPolygon> polygons;
    
    CSGNode();
    CSGNode(std::vector<CSGPolygon> inPolygons);
    ~CSGNode();
    
    CSGNode(const CSGNode& other);
    CSGNode& operator=(CSGNode other);
    
    CSGNode* clone();
    void invert();
    std::vector<CSGPolygon> clipPolygons(std::vector<CSGPolygon> clipPolygons); //Not sure about return type here //Watch out for polygons/clipPolygons
    void clipTo(CSGNode* bsp);
    void testClipTo(CSGNode bsp, std::vector<float>& data);
    std::vector<CSGPolygon> allPolygons();
    void build(std::vector<CSGPolygon> buildPolygons);
};

#endif /* csgNode_hpp */
