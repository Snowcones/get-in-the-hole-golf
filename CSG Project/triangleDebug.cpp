//
//  triangleDebug.cpp
//  CSG Project
//
//  Created by William Henning on 7/1/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "triangleDebug.hpp"
#include <string.h>
#include "triangle.hpp"


uint32_t hashString(const char* s, uint32_t length, uint32_t seed)
{
    uint32_t hash = seed;
    for (int i=0; i<length; i++)
    {
        hash = hash * 101  +  *s++;
    }
    return hash;
}

uint32_t hashFloat(float f)
{
    uint32_t ui;
    memcpy(&ui, &f, sizeof(float));
    return ui & 0xffffff00;
}

void hashVec(glm::vec3 v, char* store)
{
    uint32_t a = hashFloat(v.x);
    uint32_t b = hashFloat(v.y);
    uint32_t c = hashFloat(v.z);
    
    uint32_t vals[3] = {a, b, c};
    memcpy(store, vals, sizeof(uint32_t) * 3);
}

void hashTri(triangle t, char* store)
{
    hashVec(t.a, store);
    hashVec(t.b, store + 12);
    hashVec(t.c, store + 24);
}

unsigned int hashTri(triangle t)
{
    char vals[36];
    hashTri(t, vals);
    return hashString(vals, 36);
}

void pV3(const glm::vec3& a) {
    printf("%f, %f, %f\n", a.x, a.y, a.z);
}