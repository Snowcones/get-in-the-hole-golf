//
//  fileSystemUtil.h
//  Demos
//
//  Created by William Henning on 8/19/15.
//  Copyright (c) 2015 William Henning. All rights reserved.
//

#ifndef __Demos__fileSystemUtil__
#define __Demos__fileSystemUtil__

#include <stdio.h>
//#include "CoreFoundation/CFBundle.h"
#include <string>

//std::string getBundleFileSource(std::string name, std::string exten);
std::string getFileSource(std::string name);

#endif /* defined(__Demos__fileSystemUtil__) */
