//
//  bspStepThroughRenderer.cpp
//  CSG Project
//
//  Created by William Henning on 5/19/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "csgPortStepThroughRenderer.hpp"
#include "csgPortRenderUtility.hpp"

//To do: Clean up this and bspStepThroughRenderer
//Do I need as many uniforms as I have?
//Can these uniforms and programs be made static?

csgPortStepThroughRenderer::csgPortStepThroughRenderer() {
    setupBuffers();
    loadAndLinkShaders();
    setColors(glm::vec4(0, 1, 0, 1), glm::vec4(0, 0, 1, 1), glm::vec4(1, 0, 0, 1));
}

void csgPortStepThroughRenderer::setupBuffers() {
    
    glGenVertexArrays(1, &nVao);
    glBindVertexArray(nVao);
    glGenBuffers(1, &nVbo);
    glBindBuffer(GL_ARRAY_BUFFER, nVbo);
    
    glEnableVertexAttribArray(nAttrib);
    glVertexAttribPointer(nAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
}

void csgPortStepThroughRenderer::setTree(CSG t) {
    currentNode = new CSGNode(t.toPolygons());
    treeLocation.clear();
    bufferTree();
}

void csgPortStepThroughRenderer::moveToBack() {
    if (currentNode->back)
    {
        treeLocation.push_back(currentNode);
        currentNode = currentNode->back;
        bufferTree();
    }
}

void csgPortStepThroughRenderer::moveToFront() {
    if (currentNode->front)
    {
        treeLocation.push_back(currentNode);
        currentNode = currentNode->front;
        bufferTree();
    }
}

void csgPortStepThroughRenderer::moveUp() {
    if (treeLocation.size() == 0)
    {
        return;
    }
    currentNode = treeLocation.back();
    treeLocation.pop_back();
    bufferTree();
}

void csgPortStepThroughRenderer::bufferTree() {
    
    //Buffer Norms
    std::vector<triangle> triangles;
    triangles = triangleListFromPolygonList(*currentNode);
    
    normalPoints = 6 * (int)triangles.size();
    
    float normData[24 * triangles.size()];
    for (int i=0; i<triangles.size(); i++)
    {
        const float nm = .1;
        
        //Put the endpoints of our debug normals into the normData array
        glm::vec3 currentVec = triangles[i].a;
        normData[24*i]     = currentVec.x;
        normData[24*i + 1] = currentVec.y;
        normData[24*i + 2] = currentVec.z;
        normData[24*i + 3] = 0.0;
        
        currentVec = triangles[i].a + triangles[i].n * nm;
        normData[24*i + 4] = currentVec.x;
        normData[24*i + 5] = currentVec.y;
        normData[24*i + 6] = currentVec.z;
        normData[24*i + 7] = 0.0;
        
        currentVec = triangles[i].b;
        normData[24*i + 8]  = currentVec.x;
        normData[24*i + 9]  = currentVec.y;
        normData[24*i + 10] = currentVec.z;
        normData[24*i + 11] = 0.0;
        
        currentVec = triangles[i].b + triangles[i].n * nm;
        normData[24*i + 12] = currentVec.x;
        normData[24*i + 13] = currentVec.y;
        normData[24*i + 14] = currentVec.z;
        normData[24*i + 15] = 0.0;
        
        currentVec = triangles[i].c;
        normData[24*i + 16] = currentVec.x;
        normData[24*i + 17] = currentVec.y;
        normData[24*i + 18] = currentVec.z;
        normData[24*i + 19] = 0.0;
        
        currentVec = triangles[i].c + triangles[i].n * nm;
        normData[24*i + 20] = currentVec.x;
        normData[24*i + 21] = currentVec.y;
        normData[24*i + 22] = currentVec.z;
        normData[24*i + 23] = 0.0;
    }
    
    glBindVertexArray(nVao);
    glBindBuffer(GL_ARRAY_BUFFER, nVbo);
    glBufferData(GL_ARRAY_BUFFER, 24 * triangles.size() * sizeof(float), normData, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(nAttrib);
    glVertexAttribPointer(nAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //return;                         //////////////////////////Turns off faces
    
    //Buffer faces
    
    std::vector<triangle> frontTris;
    if(currentNode->front)
    {
        frontTris = triangleListFromPolygonList(*(currentNode->front));
    }
    
    int thisLength = 0;
    for (CSGPolygon p : currentNode->polygons)
    {
        thisLength += (p.vertices.size() - 2);
    }
    
    int frontLength    = (int)frontTris.size();
    int totalTris  = (int)triangles.size();
    verticesToDraw = 3 * totalTris;
    float posData[4 * 3 * totalTris];
    
    float z = 1.0;
    for (int i=0; i<triangles.size(); i++)
    {
        if (i >= thisLength && i < frontLength + thisLength)
        {
            z = 0.0;
        }
        else if (i >= frontLength + thisLength)
        {
            z = 2.0;
        }
        
        glm::vec3 currT = triangles[i].a;
        posData[12*i]     = currT.x;
        posData[12*i + 1] = currT.y;
        posData[12*i + 2] = currT.z;
        posData[12*i + 3] = z;
        
        currT = triangles[i].b;
        posData[12*i + 4] = currT.x;
        posData[12*i + 5] = currT.y;
        posData[12*i + 6] = currT.z;
        posData[12*i + 7] = z;
        
        currT = triangles[i].c;
        posData[12*i + 8]  = currT.x;
        posData[12*i + 9]  = currT.y;
        posData[12*i + 10] = currT.z;
        posData[12*i + 11] = z;
    }
    printf("Buffered debug tree has %d vertices\n", verticesToDraw);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, totalTris * 12 * sizeof(float), posData, GL_STATIC_DRAW);
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    //glDisableVertexAttribArray(normAttrib);
}

void csgPortStepThroughRenderer::setColors(glm::vec4 frontC, glm::vec4 thisC, glm::vec4 backC) {
    frontColor = frontC;
    thisColor = thisC;
    backColor = backC;
}

void csgPortStepThroughRenderer::render() {
    
    //Draw Normals
    GetError();
    glUseProgram(normalProg);
    glBindVertexArray(nVao);
    GetError();
    
    glUniformMatrix4fv(nModelMatUniform, 1, GL_FALSE, &modelMat[0][0]);
    GetError();
    glUniformMatrix4fv(nViewMatUniform,  1, GL_FALSE, &viewMat[0][0]);
    GetError();
    glUniformMatrix4fv(nProjMatUniform,  1, GL_FALSE, &projMat[0][0]);
    GetError();
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    if (gNormals) glDrawArrays(GL_LINES, 0, normalPoints);
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    
    GetError();
    glUseProgram(debugProg);
    glBindVertexArray(vao);
    GetError();
    
    glUniform4fv(frontColorUniform, 1, &frontColor[0]);
    glUniform4fv(thisColorUniform,  1, &thisColor[0]);
    glUniform4fv(backColorUniform,  1, &backColor[0]);
    glUniformMatrix4fv(dModelUniform, 1, GL_FALSE, &modelMat[0][0]);
    glUniformMatrix4fv(dViewUniform,  1, GL_FALSE, &viewMat[0][0]);
    glUniformMatrix4fv(dProjUniform,  1, GL_FALSE, &projMat[0][0]);
    GetError();
    
    if (gWireframe) {glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);}
    else {glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);}
    
    glDrawArrays(GL_TRIANGLES, 0, verticesToDraw);
    GetError();
}

void csgPortStepThroughRenderer::loadAndLinkShaders() {
    //Normal Renderer
    GLuint vertexShader   = compileShaderFile(GL_VERTEX_SHADER, "white.vsh");
    GLuint fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, "white.fsh");
    
    GLuint* thisProg = &normalProg;
    if (0 != vertexShader && 0 != fragmentShader)
    {
        *thisProg = glCreateProgram();
        
        glAttachShader(*thisProg, vertexShader);
        glAttachShader(*thisProg, fragmentShader);
        
        glBindFragDataLocation(*thisProg, 0, "fragColor");
        linkProgram(*thisProg);
        GetError();
        
        nAttrib  = glGetAttribLocation(*thisProg, "vertData");
        GetError();
        
        nViewMatUniform  = glGetUniformLocation(*thisProg, "viewMat");
        nModelMatUniform = glGetUniformLocation(*thisProg, "modelMat");
        nProjMatUniform  = glGetUniformLocation(*thisProg, "projMat");
        GetError();
        
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }
    else
    {
        throw "Shader compilation failed";
    }
    GetError();
    
    
    //Face Renderer
    vertexShader   = compileShaderFile(GL_VERTEX_SHADER, "bspDebug.vsh");
    fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, "bspDebug.fsh");
    
    thisProg = &debugProg;
    if (0 != vertexShader && 0 != fragmentShader)
    {
        *thisProg = glCreateProgram();
        
        glAttachShader(*thisProg, vertexShader);
        glAttachShader(*thisProg, fragmentShader);
        
        glBindFragDataLocation(*thisProg, 0, "fragColor");
        linkProgram(*thisProg);
        
        GetError();
        posAttrib  = glGetAttribLocation(*thisProg, "vertData");
        GetError();
        frontColorUniform = glGetUniformLocation(*thisProg, "frontColor");
        thisColorUniform  = glGetUniformLocation(*thisProg, "thisColor");
        backColorUniform  = glGetUniformLocation(*thisProg, "backColor");
        dViewUniform  = glGetUniformLocation(*thisProg, "viewMat");
        dModelUniform = glGetUniformLocation(*thisProg, "modelMat");
        dProjUniform  = glGetUniformLocation(*thisProg, "projMat");
        GetError();
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        GetError();
    }
    else
    {
        throw "Shader compilation failed";
    }
    GetError();
}