//
//  expressionTree.cpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "expressionTree.hpp"

ExpressionNode::ExpressionNode(CSGOperation inOp, CSG* inLeftShape, CSG* inRightShape) {
    assert((inOp == K_INVERT && inRightShape == NULL) || (inOp != K_INVERT && inRightShape != NULL));
    op = inOp;
    leftShape  = new CSGNode(inLeftShape->toPolygons());
    if(inRightShape == NULL) rightShape = NULL;
    if(inRightShape != NULL) rightShape = new CSGNode(inRightShape->toPolygons());
    leftExpression = NULL;
    rightExpression = NULL;
}

ExpressionNode::ExpressionNode(CSGOperation inOp, ExpressionNode* inLeft, ExpressionNode* inRight) {
    assert((inOp == K_INVERT && inRight == NULL) || (inOp != K_INVERT && inRight != NULL));
    op = inOp;
    leftExpression = inLeft;
    rightExpression = inRight;
    leftShape = NULL;
    rightShape = NULL;
}

ExpressionNode::~ExpressionNode() {
    if (leftExpression)
    {
        delete leftExpression;
        leftExpression = NULL;
    }
    if (rightExpression)
    {
        delete rightExpression;
        rightExpression = NULL;
    }
    if (leftShape)
    {
        delete leftShape;
        leftShape = NULL;
    }
    if (rightShape)
    {
        delete rightShape;
        rightShape = NULL;
    }
}

CSG ExpressionNode::getShape() {
    if (op == K_INVERT)   return leftSide().inverse();
    if (op == K_AND)      return leftSide().intersect(CSG(rightSide()));
    if (op == K_OR)       return leftSide().unionOf(CSG(rightSide()));
    if (op == K_SUBTRACT) return leftSide().subtract(CSG(rightSide()));
    else assert(0);
}

CSG ExpressionNode::leftSide() {
    assert((leftExpression || leftShape) && (!leftExpression || !leftShape));
    
    if (leftExpression) return leftExpression->getShape();
    else return CSG(leftShape->allPolygons());
}

CSG ExpressionNode::rightSide() {
    assert((rightExpression || rightShape) && (!rightExpression || !rightShape));
    
    if (rightExpression) return rightExpression->getShape();
    else return CSG(rightShape->allPolygons());
}

//Evaluates boolean tests on point against original shapes
bool ExpressionNode::testPoint(glm::vec3 p) {
    if (op == K_INVERT)   return !testPointLeft(p);
    if (op == K_AND)      return testPointLeft(p) && testPointRight(p);
    if (op == K_OR)       return testPointLeft(p) || testPointRight(p);
    if (op == K_SUBTRACT) return testPointLeft(p) && !testPointRight(p);
    else assert(0);
}

bool ExpressionNode::testPointLeft(glm::vec3 p) {
    assert((leftExpression || leftShape) && (!leftExpression || !leftShape));
    
    if (leftExpression)
    {
        return leftExpression->testPoint(p);
    }
    else
    {
        return pointIsInBSPTree(p, *leftShape);
    }
}

bool ExpressionNode::testPointRight(glm::vec3 p) {
    assert((rightExpression || rightShape) && (!rightExpression || !rightShape));
    
    if (rightExpression)
    {
        return rightExpression->testPoint(p);
    }
    else
    {
        return pointIsInBSPTree(p, *rightShape);
    }
}

ExpressionNode* randomExpressionTree(int layers) {
    layers = layers - 1;
    ExpressionNode* e;
    CSGOperation op = CSGOperation(rand()%4);
    if (layers > 0)
    {
        ExpressionNode* left  = NULL;
        ExpressionNode* right = NULL;
        left = randomExpressionTree(layers-1);
        if (op != K_INVERT) right = randomExpressionTree(layers-1);
        e = new ExpressionNode(op, left, right);
        return e;
    }
    else
    {
        CSG* leftShape  = NULL;
        CSG* rightShape = NULL;
        leftShape = randomShape();
        if (op != K_INVERT) rightShape = randomShape();
        e = new ExpressionNode(op, leftShape, rightShape);
        delete leftShape;
        delete rightShape;
        return e;
    }
}

CSG* randomShape() {
    CSG* s = new CSG();
    glm::vec3 scale((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
    glm::vec3 rot = 2 * 3.1415f * glm::vec3((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
    glm::vec3 center((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
    *s = CSGShapes::cube(scale, rot, center);
    return s;
}