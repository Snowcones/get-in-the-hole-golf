//
//  csgUtilities.cpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include <stdio.h>
#include "csg.hpp"

bool bspTreeNode::isPositiveDegenerate() {
    return (triNode.n == glm::vec3(0));
}

bool bspTreeNode::isNegativeDegenerate() {
    return (triNode.d < K_NULL_TRI_BOUND);
}

void bspTreeNode::copyNode(bspTreeNode* t) {
    triNode = t->triNode;
    if (t->backSubtree)
    {
        backSubtree = new bspTreeNode();
        backSubtree->copyNode(t->backSubtree);
    }
    if (t->frontSubtree)
    {
        frontSubtree = new bspTreeNode();
        frontSubtree->copyNode(t->frontSubtree);
    }
}

bspTreeNode::bspTreeNode(const triangle& t) {
    backSubtree = NULL;
    frontSubtree = NULL;
    triNode = t;
}

bspTreeNode::bspTreeNode(const bspTreeNode& t) {
    backSubtree = NULL;
    frontSubtree = NULL;
    
    triNode = t.triNode;
    if (t.backSubtree)
    {
        backSubtree = new bspTreeNode();
        backSubtree->copyNode(t.backSubtree);
    }
    if (t.frontSubtree)
    {
        frontSubtree = new bspTreeNode();
        frontSubtree->copyNode(t.frontSubtree);
    }
}