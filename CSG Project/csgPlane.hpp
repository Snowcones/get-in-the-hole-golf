//
//  csgPlane.hpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgPlane_hpp
#define csgPlane_hpp

#include <glm/glm.hpp>
#include <vector>

class CSGVertex;
class CSGPolygon;

class CSGPlane {
public:
    glm::vec3 normal;
    float w;
    constexpr static float EPSILON = 1e-5; //Errors were greater than this
//    constexpr static float EPSILON = 1e-4; //Was good before double precision was to find norms
    
    //CSGPlane();
    CSGPlane(glm::vec3 a, glm::vec3 b, glm::vec3 c);
    CSGPlane(std::vector<CSGVertex> verts);
    CSGPlane(glm::vec3 inNormal, float inW);
    CSGPlane static fromPoints(glm::vec3 a, glm::vec3 b, glm::vec3 c);
    
    CSGPlane clone();
    CSGPlane* duplicate();
    void flip();
    
    void splitPolygon(CSGPolygon& polygon, std::vector<CSGPolygon>& coplanarFront, std::vector<CSGPolygon>& coplanarBack, std::vector<CSGPolygon>& front, std::vector<CSGPolygon>& back);
};

#endif /* csgPlane_hpp */
