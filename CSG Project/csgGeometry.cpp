//
//  csgGeometry.cpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include <stdio.h>
#include "csg.hpp"

float bspTreeNode::dist(glm::vec3 p) {
    glm::vec3 disp = p - this->triNode.a;
    return glm::dot(disp, triNode.n);
}

bool bspTreeNode::raytrace(glm::vec3 origin, glm::vec3 direction, glm::vec3& intersection) {
    glm::vec3 disp = origin - triNode.a;
    bspTreeNode* closeTree = backSubtree;
    bspTreeNode* farTree   = frontSubtree;
    if (glm::dot(disp, triNode.n) > 0)
    {
        closeTree = frontSubtree;
        farTree   = backSubtree;
    }
    if (closeTree)
    {
        if (closeTree->raytrace(origin, direction, intersection))
        {
            return true;
        }
    }
    if (this->triNode.rayIntersection(origin, direction, intersection))
    {
        return true;
    }
    if (farTree)
    {
        if (farTree->raytrace(origin, direction, intersection))
        {
            return true;
        }
    }
    return false;
}

//Transforms
//

void bspTreeNode::invert() { //Flips the inside and outside of a bspTree
    if (triNode.n == glm::vec3(0))  //If the triangle is a +null triangle, make the inverse a -null triangle
    {
        printf("Called invert on positive degenerate tree\n");
        triNode.n = glm::vec3(1);
        triNode.a = glm::vec3(K_NULL_TRI_DIST, 0, 0);
        triNode.b = glm::vec3(0, 0, K_NULL_TRI_DIST);
        triNode.c = glm::vec3(0, K_NULL_TRI_DIST, 0);
        triNode.d = K_NULL_TRI_DIST;
    }
    else if (triNode.d < K_NULL_TRI_BOUND)    //If the triangle is a -null triangle, make the inverse a +null triangle
    {
        printf("Called invert on negative degenerate tree\n");
        triNode.n = glm::vec3(0);
        triNode.a = glm::vec3(0);
        triNode.b = glm::vec3(0);
        triNode.c = glm::vec3(0);
        triNode.d = 0;
    }
    else                            //If the triangle is normal, just invert it
    {
        this->uncheckedInvert();
    }
}

void bspTreeNode::uncheckedInvert() {
    triNode.n *= -1;
    triNode.d *= -1;
    
    std::swap(frontSubtree, backSubtree);
    if (frontSubtree)
    {
        frontSubtree->uncheckedInvert();
    }
    if (backSubtree)
    {
        backSubtree->uncheckedInvert();
    }
}

void bspTreeNode::translate(glm::vec3 t) {
    triNode.a += t;
    triNode.b += t;
    triNode.c += t;
    triNode.recalculateD(); //Won't break flipped triangles because doesn't recalc norm
    
    if (frontSubtree)
    {
        frontSubtree->translate(t);
    }
    if (backSubtree)
    {
        backSubtree->translate(t);
    }
}

void bspTreeNode::scale(glm::vec3 s) {
    triNode.a *= s;
    triNode.b *= s;
    triNode.c *= s;
    triNode.n *= s;
    triNode.n = glm::normalize(triNode.n);
    triNode.recalculateD();
    
    if (frontSubtree)
    {
        frontSubtree->scale(s);
    }
    if (backSubtree)
    {
        backSubtree->scale(s);
    }
}


//Triangle Lists
//

void bspTreeNode::getTriangleList(std::vector<triangle> &list) {
    if (triNode.n == glm::vec3(0))
    {
        printf("Called getTriList on positive degenerate tree\n");
        return;
    }
    else if (triNode.d < K_NULL_TRI_BOUND)
    {
        printf("Called getTriList on negative degenerate tree\n");
        return;
    }
    else
    {
        this->uncheckedGetTriList(list);
    }
}

void bspTreeNode::getTriangleList(int* trianglesToDraw, float **data) {
    std::vector<triangle> triangles;
    this->getTriangleList(triangles);
    
    *trianglesToDraw = (int)triangles.size();
    int verticesNum = (int)(3*triangles.size());
    int elementsNum = 6 * verticesNum;
    int vertexDataSize = elementsNum * sizeof(float);
    
    *data = (float*)malloc(vertexDataSize);
    float* vertexData = *data;
    
    for (int i=0; i<triangles.size(); i++)
    {
        vertexData[18*i] = triangles[i].a.x;
        vertexData[18*i + 1] = triangles[i].a.y;
        vertexData[18*i + 2] = triangles[i].a.z;
        
        vertexData[18*i + 6] = triangles[i].b.x;
        vertexData[18*i + 7] = triangles[i].b.y;
        vertexData[18*i + 8] = triangles[i].b.z;
        
        vertexData[18*i + 12] = triangles[i].c.x;
        vertexData[18*i + 13] = triangles[i].c.y;
        vertexData[18*i + 14] = triangles[i].c.z;
        
        for (int j=0; j<3; j++)
        {
            vertexData[18*i + j*6 + 3] = triangles[i].n.x;
            vertexData[18*i + j*6 + 4] = triangles[i].n.y;
            vertexData[18*i + j*6 + 5] = triangles[i].n.z;
        }
    }
}


void bspTreeNode::uncheckedGetTriList(std::vector<triangle> &list) {
    //triNode.recalculateNorm();
    list.push_back(triNode);
    if (frontSubtree)
    {
        frontSubtree->uncheckedGetTriList(list);
    }
    if (backSubtree)
    {
        backSubtree->uncheckedGetTriList(list);
    }
}