//
//  triangle.hpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef triangle_hpp
#define triangle_hpp

#include <stdio.h>
#include <glm/glm.hpp>

#ifdef DEBUG
#include "triangleDebug.hpp"
#endif

struct triangle {
    triangle();
    triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c);
    
    glm::vec3 a;
    glm::vec3 b;
    glm::vec3 c;
    glm::vec3 n;
    float d;
    float A;
    
    #ifdef DEBUG
    uint32_t debugHash;
    #endif
    
    void recalculateNorm();
    void recalculateD();
    float dist(glm::vec3 p);
    bool rayIntersection(glm::vec3 origin, glm::vec3 direction, glm::vec3& intersection);
    glm::vec3 calculateIntersection(glm::vec3 p1, glm::vec3 p2);
    bool operator==(const triangle &other) const;
    bool operator!=(const triangle &other) const;
};

void calculateIntersectionPoints(triangle splitter, triangle splittee, glm::vec3& A, glm::vec3& B);
void calculateSingleIntersection(triangle splitter, glm::vec3 endpoint1, glm::vec3 endpoint2, glm::vec3& P);

#endif /* triangle_hpp */
