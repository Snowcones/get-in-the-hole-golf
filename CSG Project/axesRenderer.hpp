//
//  axesRenderer.hpp
//  CSG Project
//
//  Created by William Henning on 5/20/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef axesRenderer_hpp
#define axesRenderer_hpp

#include <stdio.h>
#include "diffuseRenderer.hpp"

class axesRenderer
{
protected:
    static void setupBuffers();
    static void loadAndLinkShaders();
    static void updateViewingMatrices();
    
    static glm::vec2 orientation;
    static glm::vec3 translation;
    static glm::vec4 color;
    
public:
    static void setup();
    static void render();
    static void setOrientation(glm::vec2 o);
    static void setTranslation(glm::vec3 t);
    static void rotate(glm::vec2 r);
    static void translate(glm::vec3 t);
    
    static int verticesToDraw;
    static GLuint vao;
    static GLuint vbo;
    static GLuint dataAttrib;
    static GLuint axesProg;    //Could be static
    
    static GLuint xColorUniform;
    static GLuint yColorUniform;
    static GLuint zColorUniform;
    
    static GLuint modelMatUniform;
    static GLuint viewMatUniform;
    static GLuint projMatUniform;
    
    static glm::mat4 modelMat;
    static glm::mat4 viewMat;
    static glm::mat4 projMat;
    
    static int screenWidth;
    static int screenHeight;
};

#endif /* axesRenderer_hpp */
