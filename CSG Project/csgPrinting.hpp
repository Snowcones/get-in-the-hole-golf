//
//  csgPrinting.hpp
//  CSG Project
//
//  Created by William Henning on 5/12/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgPrinting_hpp
#define csgPrinting_hpp

#include <stdio.h>
#include "csg.hpp"

void printBSPTree(bspTreeNode* tree);
void printFlatTree(bspTreeNode* n);

#endif /* csgPrinting_hpp */
