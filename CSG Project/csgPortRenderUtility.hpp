//
//  csgPortRenderUtility.hpp
//  CSG Project
//
//  Created by William Henning on 6/15/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgPortRenderUtility_hpp
#define csgPortRenderUtility_hpp

#include <stdio.h>
#include "csgPort.hpp"
#include <vector>
#include "triangle.hpp"

void getTriangleList(CSG tree, int* numTrisToDraw, float** dataStore);
std::vector<triangle> triangleListFromPolygonList(CSGNode tree);

#endif /* csgPortRenderUtility_hpp */
