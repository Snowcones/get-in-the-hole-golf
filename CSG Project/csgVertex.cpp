//
//  csgVertex.cpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "csgVertex.hpp"
#include <glm/gtx/compatibility.hpp>

CSGVertex::CSGVertex(glm::vec3 inPos, glm::vec3 inNormal) {
    pos = inPos;
    normal = inNormal;
}

CSGVertex CSGVertex::clone() {
    return CSGVertex(pos, normal);
}

void CSGVertex::flip() {
    normal *= -1;
}

CSGVertex CSGVertex::interpolate(CSGVertex other, float t) {
    return CSGVertex(glm::lerp(pos, other.pos, t),
                     glm::lerp(normal, other.normal, t));
}