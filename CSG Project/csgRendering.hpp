//
//  csgRendering.hpp
//  CSG Project
//
//  Created by William Henning on 5/15/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgRendering_hpp
#define csgRendering_hpp

//Stuff needed
#include <stdio.h>

//Renderers
#include "diffuseRenderer.hpp"
#include "bspRenderer.hpp"
#include "bspStepThroughRenderer.hpp"
#include "csgPortStepThroughRenderer.hpp"
#include "axesRenderer.hpp"

void printFPS();

extern bool gWireframe;
extern bool gNormals;

#endif /* csgRendering_hpp */
