//
//  csgPrinting.cpp
//  CSG Project
//
//  Created by William Henning on 5/12/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "csgPrinting.hpp"
#include <vector>
#include <iostream>
#include <string>

int maxLevel(bspTreeNode* tree) {
    if (tree == NULL)
    {
        return 0;
    }
    else
    {
        int a = maxLevel(tree->backSubtree)+1;
        int b = maxLevel(tree->frontSubtree)+1;
        
        if (a>b)
        {
            return a;
        }
        else
        {
            return b;
        }
    }
}

void printSpaces(int n) {
    for (int i=0; i<n; i++)
    {
        printf(" ");
    }
}

bool allNodesAreNull(std::vector<bspTreeNode*> nodes)
{
    for (int i=0; i<nodes.size(); i++)
    {
        if (nodes[i] != NULL)
        {
            return false;
        }
    }
    return true;
}

void printBSPTreeInternal(std::vector<bspTreeNode*> nodes, int level, int levels) {
    if (nodes.size() == 0 || allNodesAreNull(nodes))
    {
        return;
    }
    
    int floor = levels - level;
    int clampedFloor = floor - 1;
    if (clampedFloor<0) {clampedFloor = 0;}
    int edgeLines = (int)pow(2, clampedFloor);
    int firstSpaces = (int)pow(2, floor) - 1;
    int betweenSpaces = (int)pow(2, floor+1) - 1;
    
    printSpaces(firstSpaces);
    
    std::vector<bspTreeNode*> newNodes;
    for (bspTreeNode* node : nodes)
    {
        if (node != NULL)
        {
            printf("O");
            newNodes.push_back(node->backSubtree);
            newNodes.push_back(node->frontSubtree);
        } else {
            printf(" ");
            newNodes.push_back(NULL);
            newNodes.push_back(NULL);
        }
        printSpaces(betweenSpaces);
    }
    printf("\n");
    
    for (int i=1; i<=edgeLines; i++)
    {
        for (int j=0; j < nodes.size(); j++)
        {
            printSpaces(firstSpaces - i);
            if (nodes[j] == NULL)
            {
                printSpaces(2*edgeLines + i + 1);
                continue;
            }
            
            if (nodes[j]->backSubtree)
            {
                printf("/");
            } else {
                printSpaces(1);
            }
            
            printSpaces(2*i - 1);
            
            if (nodes[j]->frontSubtree)
            {
                printf("\\");
            } else {
                printSpaces(1);
            }
            
            printSpaces(2 * edgeLines - i);
        }
        printf("\n");
    }
    
    printBSPTreeInternal(newNodes, level + 1, levels);
}

void printBSPTree(bspTreeNode* tree) {
    int levels = maxLevel(tree);
    printBSPTreeInternal({tree}, 1, levels);
}

std::string pointOut(glm::vec3 p) {
    int MAXLEN = 3*20 + 1; //No idea what size to use
    char string[MAXLEN];
    snprintf(string, MAXLEN, "%.2f, %.2f, %.2f", p.x, p.y, p.z);
    return std::string(string);
}

std::string triOut(triangle t) {
    int MAXLEN = 3 * 3*20 + 12; //No idea what size to use
    char string[MAXLEN];
    snprintf(string, MAXLEN, "(%s), (%s), (%s)", pointOut(t.a).c_str(), pointOut(t.b).c_str(), pointOut(t.c).c_str());
    return std::string(string);
}

void printFlatTreeInternal(std::string prefix, bool isTail, bspTreeNode* n) {
    std::string text = triOut(n->triNode)+" "+pointOut(n->triNode.n);
    //std::string text = pointOut(n->triNode.n);
    //std::string text = "O";
    
    std::cout<<(prefix + (isTail ? "└── " : "├── ") + text)<<std::endl;
    if(n->frontSubtree && !n->backSubtree)
    {
        printFlatTreeInternal(prefix + (isTail ? "    " : "│   "), true, n->frontSubtree);
    }
    else if(!n->frontSubtree && n->backSubtree)
    {
        printFlatTreeInternal(prefix + (isTail ? "    " : "│   "), true, n->backSubtree);
    }
    else if (n->frontSubtree && n->backSubtree)
    {
        printFlatTreeInternal(prefix + (isTail ? "    " : "│   "), false, n->frontSubtree);
        printFlatTreeInternal(prefix + (isTail ? "    " : "│   "), true, n->backSubtree);
    }
}

void printFlatTree(bspTreeNode* n) {
    printFlatTreeInternal("", true, n);
}
