//
//  triangleDebug.hpp
//  CSG Project
//
//  Created by William Henning on 7/1/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef triangleDebug_hpp
#define triangleDebug_hpp

#include <stdio.h>
#include <glm/glm.hpp>
#include <string>

class triangle;

uint32_t hashString(const char* s, uint32_t length, uint32_t seed = 0);
uint32_t hashFloat(float f);
void hashVec(glm::vec3 v, char* store);
void hashTri(triangle t, char* store);
uint32_t hashTri(triangle t);

void pV3(const glm::vec3& a); //Should probably be in a different file

#endif /* triangleDebug_hpp */
