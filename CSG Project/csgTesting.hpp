//
//  csgTesting.hpp
//  CSG Project
//
//  Created by William Henning on 5/15/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgTesting_hpp
#define csgTesting_hpp

#include <stdio.h>
#include "csg.hpp"
#include "csgPort.hpp"
#include "csgPrinting.hpp"

bool pointIsInBSPTree(glm::vec3 p, bspTreeNode* n);
bool pointIsInBSPTree(glm::vec3 p, const CSGNode& t);

#endif /* csgTesting_hpp */
