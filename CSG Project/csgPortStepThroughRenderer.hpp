//
//  csgPortStepThroughRenderer.hpp
//  CSG Project
//
//  Created by William Henning on 5/19/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgPortStepThroughRenderer_hpp
#define csgPortStepThroughRenderer_hpp

#include <stdio.h>
#include "diffuseRenderer.hpp"
#include "csgPort.hpp"

extern bool gWireframe;
extern bool gNormals;

class csgPortStepThroughRenderer : public diffuseRenderer {
private:
    
    glm::vec4 frontColor;
    glm::vec4 thisColor;
    glm::vec4 backColor;
    
    int normalPoints;
    
    GLuint nVao;
    GLuint nVbo;
    GLuint debugProg;
    GLuint normalProg;
    GLuint nAttrib;
    
    GLuint frontColorUniform;
    GLuint thisColorUniform;
    GLuint backColorUniform;
    
    GLuint dModelUniform;
    GLuint dViewUniform;
    GLuint dProjUniform;
    
    GLuint nModelMatUniform;
    GLuint nViewMatUniform;
    GLuint nProjMatUniform;
    
    void loadAndLinkShaders();
    void setupBuffers();
    void bufferTree();
    
public:
    CSGNode* currentNode;
    std::vector<CSGNode*> treeLocation;
    
    
    csgPortStepThroughRenderer();
    void render();
    void setTree(CSG t);
    void setColors(glm::vec4 frontC, glm::vec4 thisC, glm::vec4 backC);
    void moveToBack();
    void moveToFront();
    void moveUp();
    
    void testTrees(bspTreeNode* t1, bspTreeNode* t2);
};

#endif /* csgPortStepThroughRenderer_hpp */
