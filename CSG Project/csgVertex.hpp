//
//  csgVertex.hpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef csgVertex_hpp
#define csgVertex_hpp

#include <glm/glm.hpp>

class CSGVertex {
public:
    glm::vec3 pos;
    glm::vec3 normal;

    CSGVertex clone();
    CSGVertex() = default;
    CSGVertex(glm::vec3 inPos, glm::vec3 inNormal);
    void flip();
    CSGVertex interpolate(CSGVertex other, float t);
};

#endif /* csgVertex_hpp */
