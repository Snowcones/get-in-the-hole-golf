//
//  diffuseRenderer.cpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "diffuseRenderer.hpp"

int diffuseRenderer::screenWidth = 800;
int diffuseRenderer::screenHeight = 600;
bool diffuseRenderer::isLoaded = false;

GLuint diffuseRenderer::posAttrib;
GLuint diffuseRenderer::normAttrib;
GLuint diffuseRenderer::prog;

GLuint diffuseRenderer::colorUniform;
GLuint diffuseRenderer::modelMatUniform;
GLuint diffuseRenderer::viewMatUniform;
GLuint diffuseRenderer::projMatUniform;

void diffuseRenderer::loadAndLinkShaders() {
    if (!isLoaded)
    {
        isLoaded = true;
        GetError();
        GLuint vertexShader   = compileShaderFile(GL_VERTEX_SHADER, "diffuse.vsh");
        GLuint fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, "diffuse.fsh");
        
        if (0 != vertexShader && 0 != fragmentShader)
        {
            prog = glCreateProgram();
            
            glAttachShader(prog, vertexShader);
            glAttachShader(prog, fragmentShader);
            
            glBindFragDataLocation(prog, 0, "fragColor");
            linkProgram(prog);
            
            posAttrib  = glGetAttribLocation(prog, "pos");
            normAttrib = glGetAttribLocation(prog, "norm");
            
            colorUniform = glGetUniformLocation(prog, "color");
            viewMatUniform  = glGetUniformLocation(prog, "viewMat");
            modelMatUniform = glGetUniformLocation(prog, "modelMat");
            projMatUniform  = glGetUniformLocation(prog, "projMat");
            
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);
        }
        else
        {
            throw "Shader compilation failed";
        }
        glUseProgram(prog);
        GetError();
    }
}


diffuseRenderer::diffuseRenderer() {
    loadAndLinkShaders();
    setupBuffers();
    setRenderColor(glm::vec4(1.0));
    
    orientation = glm::vec2(0, 0);
    translation = glm::vec3(0, 0, -3);
    updateViewingMatrices();
    GetError();
}
void diffuseRenderer::setupBuffers() {
    GetError();
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), 0);

    glEnableVertexAttribArray(normAttrib);
    glVertexAttribPointer(normAttrib, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (GLvoid*)(3*sizeof(float)));
    GetError();
}

void diffuseRenderer::updateViewingMatrices() {
    modelMat = glm::mat4();
    modelMat = glm::eulerAngleYX(orientation.x, orientation.y);
    
    viewMat = glm::mat4();
    viewMat = glm::translate(viewMat, glm::vec3(translation));

    projMat = glm::perspective(3.1415f/2, (float)screenWidth/screenHeight, .2f, 40.0f);
}

void diffuseRenderer::setOrientation(glm::vec2 o) {
    orientation = o;
    updateViewingMatrices();
}

void diffuseRenderer::rotate(glm::vec2 r) {
    orientation += r;
    updateViewingMatrices();
}

void diffuseRenderer::setTranslation(glm::vec3 t) {
    translation = t;
    updateViewingMatrices();
}

void diffuseRenderer::translate(glm::vec3 t) {
    translation += t;
    updateViewingMatrices();
}

void diffuseRenderer::setRenderColor(glm::vec4 c) {
    color = c;
    glUseProgram(prog);
    glUniform4fv(colorUniform, 1, &color[0]);
}

void diffuseRenderer::render() {
    glUseProgram(prog);
    glBindVertexArray(vao);
    
    glUniform4fv(colorUniform, 1, &color[0]);
    glUniformMatrix4fv(modelMatUniform, 1, GL_FALSE, &modelMat[0][0]);
    glUniformMatrix4fv(viewMatUniform,  1, GL_FALSE, &viewMat[0][0]);
    glUniformMatrix4fv(projMatUniform,  1, GL_FALSE, &projMat[0][0]);
    GetError();
    
    glDrawArrays(GL_TRIANGLES, 0, verticesToDraw);
    GetError();
}

void diffuseRenderer::bufferTriList(int numTriangles, float *triangleData) {
    GetError();
    int verticiesNum = (int)(3*numTriangles);
    int elementsNum = 6 * verticiesNum;
    int verticiesSize = elementsNum * sizeof(float);
    
    verticesToDraw = verticiesNum;
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, verticiesSize, triangleData, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), 0);
    
    glEnableVertexAttribArray(normAttrib);
    glVertexAttribPointer(normAttrib, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (GLvoid*)(3*sizeof(float)));
    GetError();
}

glm::vec3 diffuseRenderer::getTranslation() {
    return translation;
}

glm::vec2 diffuseRenderer::getOrientation() {
    return orientation;
}