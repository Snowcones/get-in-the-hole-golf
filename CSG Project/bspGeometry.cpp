//
//  bspGeometry.cpp
//  CSG Project
//
//  Created by William Henning on 5/12/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "bspGeometry.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

glm::mat4 transformMat(glm::vec3 scale, glm::vec3 rot, glm::vec3 center) {
    glm::mat4 transform = glm::mat4();
    transform = glm::translate(transform, center) * glm::eulerAngleYXZ(rot.x, rot.y, rot.z) * glm::scale(transform, scale);
    
    return transform;
}

void transformFloatList(float* data, int numPoints, const glm::mat4& transform)
{
    for (int i=0; i<numPoints; i++)
    {
        glm::vec4 p = glm::vec4(data[3*i], data[3*i + 1], data[3*i + 2], 1.0);
        p = transform * p;
        data[3*i]     = p.x;
        data[3*i + 1] = p.y;
        data[3*i + 2] = p.z;
    }
}

void transformTriList(std::vector<triangle>& tris, const glm::mat4& transform)
{
    for (triangle t : tris)
    {
        glm::vec4 a = (transform * glm::vec4(t.a, 1.0));
        glm::vec4 b = (transform * glm::vec4(t.b, 1.0));
        glm::vec4 c = (transform * glm::vec4(t.c, 1.0));
        glm::vec4 n = (transform * glm::vec4(t.n, 0.0));
        t.a = glm::vec3(a.x, a.y, a.z);
        t.b = glm::vec3(b.x, b.y, b.z);
        t.c = glm::vec3(c.x, c.y, c.z);
        t.n = glm::normalize(glm::vec3(n.x, n.y, n.z));
        t.recalculateD();
    }
}

bspTreeNode* treeFromTriList(std::vector<triangle> tris) {
    bspTreeNode* out = new bspTreeNode();
    if (tris.size()>0)
    {
        out->triNode = tris[0];
        for (int i=1; i<tris.size(); i++)
        {
            out->add(tris[i]);
        }
    }
    return out;
}

bspTreeNode* bspCube(glm::vec3 scale, glm::vec3 rot, glm::vec3 center) {
    
    /* Cube Geometry Reference

       3----5
      /|   /|
     1----7 |
     | 2--|-4
     |/   |/
     0----6
     
     */
    
    glm::mat4 transform = transformMat(scale, rot, center);
    const int numPoints = 8*6;
    
    float cubeData[3 * numPoints]={
        -1, -1,  1,      1, -1,  1,      1,  1,  1,     -1,  1,  1,     //v0-v6-v7-v1 Front
        -1, -1, -1,     -1, -1,  1,     -1,  1,  1,     -1,  1, -1,     //v2-v0-v1-v3 Left
         1, -1, -1,     -1, -1, -1,     -1,  1, -1,      1,  1, -1,     //v4-v2-v3-v5 Back
         1, -1,  1,      1, -1, -1,      1,  1, -1,      1,  1,  1,     //v6-v4-v5-v7 Right
        -1,  1,  1,      1,  1,  1,      1,  1, -1,     -1,  1, -1,     //v1-v7-v5-v3 Top
        -1, -1, -1,      1, -1, -1,      1, -1,  1,     -1, -1,  1};   //v2-v4-v6-v0 Bottom
    
    
    if (scale != glm::vec3(1.0) || rot != glm::vec3(0))
    {
        transformFloatList(cubeData, numPoints, transform);
    }
    
    uint32_t indexList[6*6]={
        0,  1,  2,      0,  2,  3,      //Front
        4,  5,  6,      4,  6,  7,      //Left
        8,  9, 10,      8, 10, 11,      //Back
        12, 13, 14,     12, 14, 15,     //Right
        16, 17, 18,     16, 18, 19,     //Top
        20, 21, 22,     20, 22, 23};    //Bottom
    
    triangle cubeTris[12];
    
    for (int i=0; i<12; i++)
    {
        int dataIndicies[3];
        for (int j=0; j<3; j++)
        {
            dataIndicies[j] = indexList[3*i + j];
        }
        
        int l = dataIndicies[0];
        glm::vec3 a = glm::vec3(cubeData[3*l], cubeData[3*l + 1], cubeData[3*l + 2]);
        l = dataIndicies[1];
        glm::vec3 b = glm::vec3(cubeData[3*l], cubeData[3*l + 1], cubeData[3*l + 2]);
        l = dataIndicies[2];
        glm::vec3 c = glm::vec3(cubeData[3*l], cubeData[3*l + 1], cubeData[3*l + 2]);
        
        cubeTris[i] = triangle(a, b, c);
    }
    
    bspTreeNode* tree = new bspTreeNode();
    tree->triNode = cubeTris[0];
    
    for (int i=1; i<12; i++)
    {
        tree->add(cubeTris[i]);
    }
    return tree;
}

void genSphereTriList(std::vector<triangle>& triList, float radius = 1.0, glm::vec3 center = glm::vec3(0), int thetaRes = 20, int phiRes = 10) {
    triList.clear();

    for (int rot=0; rot<thetaRes; rot++)
    {
        for (int incl=0; incl<phiRes; incl++)
        {
            //Sphere is a patch of squares
            /*
             D--------C
             |     /  |
             |    /   |
             |   /    |
             |  /     |
             A--------B
             */
            
            
            float rotAngle  = 2*M_PI*rot / thetaRes;
            float inclAngle = -M_PI_2 + M_PI*incl / phiRes;
            float nextRotAngle = 2*M_PI*(rot+1) / thetaRes;
            float nextInclAngle = -M_PI_2 + M_PI*(incl+1) / phiRes;
            
            float xPos =  radius * cosf(rotAngle) * cosf(inclAngle);
            float yPos =  radius * sinf(inclAngle);
            float zPos =  radius * -sinf(rotAngle) * cosf(inclAngle);
            
            float currHighXPos = radius *  cosf(rotAngle) * cosf(nextInclAngle);
            float currHighZPos = radius * -sinf(rotAngle) * cosf(nextInclAngle);
            float nextLowXPos  = radius *  cosf(nextRotAngle) * cosf(inclAngle);
            float nextLowZPos  = radius * -sinf(nextRotAngle) * cosf(inclAngle);
            float nextHighXPos = radius *  cosf(nextRotAngle) * cosf(nextInclAngle);
            float nextHighZPos = radius * -sinf(nextRotAngle) * cosf(nextInclAngle);
            
            float nextYPos = radius * sinf(nextInclAngle);
            
            glm::vec3 a = glm::vec3(xPos, yPos, zPos);
            glm::vec3 b = glm::vec3(nextLowXPos, yPos, nextLowZPos);
            glm::vec3 c = glm::vec3(nextHighXPos, nextYPos, nextHighZPos);
            glm::vec3 d = glm::vec3(currHighXPos, nextYPos, currHighZPos);
            
            a += center;
            b += center;
            c += center;
            d += center;
            
            if (incl != 0)
            {
                triList.push_back(triangle(a, b, c));
            }
            if (incl != phiRes - 1)
            {
                triList.push_back(triangle(a, c, d));
            }
        }
    }
}

bspTreeNode* bspSphere(float radius, glm::vec3 center, int thetaRes, int phiRes) {
    std::vector<triangle> sphereTris;
    genSphereTriList(sphereTris, radius, center, thetaRes, phiRes);
    return treeFromTriList(sphereTris);
}

bspTreeNode* bspSpheroid(glm::vec3 scale, glm::vec3 rot, glm::vec3 center, int thetaRes, int phiRes) {
    std::vector<triangle> sphereTris;
    genSphereTriList(sphereTris, 1.0, center, thetaRes, phiRes);
    
    glm::mat4 transform = transformMat(scale, rot, center);
    transformTriList(sphereTris, transform);
    return treeFromTriList(sphereTris);
}
