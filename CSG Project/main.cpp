//
//  main.cpp
//  CSG Project
//
//  Created by William Henning on 3/1/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include <SDL2/SDL.h>
#include <iostream>
#include "csg.hpp"
#include "csgPrinting.hpp"
#include "csgTesting.hpp"
#include "csgRendering.hpp"
#include "bspGeometry.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "expressionTree.hpp"

#include <ctime>
#include <unistd.h>

//Please, please, please, make diffuseRenderer, bspRenderer, bspStepThroughRenderer share a well designed baseRenderer

int screenWidth = 800;
int screenHeight = 600;


int main(int argc, const char * argv[]) {
    #ifdef DEBUG
    printf("In Debug Mode\n");
    #endif

    if(SDL_Init(SDL_INIT_VIDEO)<0) {printf("Failed to initialize video\n");}
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    SDL_Window *window=SDL_CreateWindow("Get in the Hole Golf", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
    SDL_GLContext context=SDL_GL_CreateContext(window);
    SDL_GL_MakeCurrent(window, context);

    glewInit();

    bool manualVsync; //Controls vsync for unsupported systems
    if (context==NULL)
    { printf("Unable to create OpenGL context\n");
      assert(-1); }
    { if(SDL_GL_SetSwapInterval(1)<0) { //Sets vsync
            printf("Vsync unsupported setting manual framerate control\n");
            manualVsync=true;
            assert(-2); }
    }
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glClearColor(1.0, 1.0, 1.4, 1);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    pointCloud p;
    p.shape = CLOUD_SPHERE;
    p.r = 2;

    bspRenderer r;
    bspRenderer r2;
    bspRenderer r3;

    //CSG t1 = CSGShapes::cube();
    CSG t1 = CSGShapes::sphere(glm::vec3(1.4));
    CSG t2 = CSGShapes::cube(glm::vec3(1, 1, 1), glm::vec3(M_PI_4, M_PI_4, 0));
    CSG t3 = t1.intersect(t2);

    r.bufferBSPTree(t1);
    r2.bufferBSPTree(t2);

    r.setRenderColor(glm::vec4(1, 0, 1, .5));
    r2.setRenderColor(glm::vec4(0, 1, 0, .5));

    r3.setRenderColor(glm::vec4(1));
    r3.setupVolumeTest(p, 50000, t3);
    r3.setColors(glm::vec4(0.0, 1.0, 0.0, 1.0), glm::vec4(0.0, 0.0, 0.0, 0.0));

    csgPortStepThroughRenderer d1;
    d1.setTree(t3);
    r3.renderOpaque();

    axesRenderer::setup();

    bool running=true;
    SDL_Event e;
    uint fakeTimer = 0;
    const int repeatTime = 5;
    while (running)
    {
        fakeTimer++;
        while( SDL_PollEvent(&e) != 0 )
        {
            if( e.type == SDL_QUIT )
            {
                running = false;
            }
            if(e.type==SDL_MOUSEWHEEL)
            {   //Zoom

            }
            if (e.type==SDL_MOUSEBUTTONDOWN)
            {
                if (e.button.button == SDL_BUTTON_LEFT)
                {
                    float x = e.button.x;
                    float y = e.button.y;

                    int height;
                    int width;
                    SDL_GetWindowSize(window, &width, &height);

                    x = x/width*2 - 1;
                    y = -y/height*2 + 1;

                    glm::vec3 clickLoc;
                    glm::vec3 origin = glm::vec3(0);

                    glm::mat4 projM = d1.projMat;
                    glm::mat4 invProj = glm::inverse(projM);
                    glm::vec4 ray_clip = glm::vec4(x, y, -1.0, 1.0);
                    glm::vec4 ray_cam  = invProj * ray_clip;
                    glm::vec3 direction = glm::vec3(ray_cam);


                    glm::mat4 transform = glm::translate(glm::mat4(), r3.getTranslation()) * glm::eulerAngleYX(r3.getOrientation().x, r3.getOrientation().y);

                    glm::mat4 invTransform = glm::inverse(transform);

                    origin = glm::vec3(invTransform * glm::vec4(origin, 1.0));
                    direction = glm::mat3(invTransform) * direction;

//                    printf("Origin: %f, %f, %f\n", origin.x, origin.y, origin.z);
//                    printf("Direction: %f, %f, %f\n", direction.x, direction.y, direction.z);
//
//                    if(t3->raytrace(origin, direction, clickLoc)) {
//                        clickLoc = glm::vec3(transform * glm::vec4(clickLoc, 1.0));
//
//                        bspTreeNode* addCube = bspCube(glm::vec3(1), glm::vec3(0), clickLoc);
//                        t3->orCSG(addCube);
//                        r3.bufferBSPTree(t3);
//                        free(addCube);
//
//                        printf("%f, %f, %f\n", clickLoc.x, clickLoc.y, clickLoc.z);
//                    }

                }
            }
            if (e.type==SDL_MOUSEMOTION)
            {
                if (e.motion.state&&SDL_BUTTON_LMASK)
                { //Mouse drag

                    const float rotSpeed = 5.0;
                    glm::vec2 drag = glm::vec2(e.motion.xrel, -e.motion.yrel);

                    drag /= glm::vec2(screenWidth, screenHeight);
                    drag *= rotSpeed;

                    r.rotate(drag);
                    r2.rotate(drag);
                    axesRenderer::rotate(drag);
                    r3.rotate(drag);
                    d1.rotate(drag);
                }
            }
            if (e.type==SDL_KEYDOWN)
            {   //Keypress

            }
            if (e.type==SDL_WINDOWEVENT)
            {
                int height;
                int width;
                SDL_GetWindowSize(window, &width, &height);
                glViewport(0, 0, width, height);
                diffuseRenderer::screenWidth = width;
                diffuseRenderer::screenHeight = height;
            }
        }

        const Uint8* keys = SDL_GetKeyboardState(NULL); // NOT "keys = NULL;"
        if (keys[SDL_SCANCODE_LEFT] && fakeTimer>repeatTime)
        {
            d1.moveToBack();
            fakeTimer = 0;
            //r3.setupVolumeTest(p, 50000, d1.cur);
        }
        if (keys[SDL_SCANCODE_RIGHT] && fakeTimer>repeatTime)
        {
            d1.moveToFront();
            fakeTimer = 0;
        }
        if (keys[SDL_SCANCODE_UP] && fakeTimer>repeatTime)
        {
            d1.moveUp();
            fakeTimer = 0;
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//        axesRenderer::render();
        r.render();
        r2.render();
        r3.render();
        d1.render();
        SDL_GL_SwapWindow(window);
        printFPS();
        usleep(1000000/60);
    }

    return 0;
}
