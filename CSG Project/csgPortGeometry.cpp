//
//  csgPortGeometry.cpp
//  CSG Project
//
//  This code is part of my port of csg.js http://evanw.github.io/csg.js/docs/ to c++
//

#include <stdio.h>

#include "csgPort.hpp"
#include "csgPortGeometry.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

CSG CSGShapes::cube(glm::vec3 scale, glm::vec3 rot, glm::vec3 center) {
    int cubeData[6][4] = {{0, 4, 6, 2},
                            {1, 3, 7, 5},
                            {0, 1, 5, 4},
                            {2, 6, 7, 3},
                            {0, 2, 3, 1},
                            {4, 5, 7, 6}};

    float normData[6][3] = {{-1, 0, 0},
                            {+1, 0, 0},
                            {0, -1, 0},
                            {0, +1, 0},
                            {0, 0, -1},
                            {0, 0, +1}};
    glm::mat4 transform;
    transform = glm::scale(glm::mat4(), scale) * transform;
    transform = glm::eulerAngleYXZ(rot.x, rot.y, rot.z) * transform;
    transform = glm::translate(glm::mat4(), center) * transform;

    std::vector<CSGPolygon> polygons;
    for (int face = 0; face < 6; face++)
    {
        std::vector<CSGVertex> vertices;
        for (int vertex = 0; vertex < 4; vertex++)
        {
            int vertNum = cubeData[face][vertex];
            float x = (vertNum & 1) ? 1 : -1;   //Voodoo bit-hacking, gets a vertex position from an int
            float y = (vertNum & 2) ? 1 : -1;
            float z = (vertNum & 4) ? 1 : -1;
            glm::vec3 pos = glm::vec3(transform * glm::vec4(x, y, z, 1));
            glm::vec3 norm = glm::vec3(normData[face][0], normData[face][1], normData[face][2]);
            vertices.push_back(CSGVertex(pos, norm));
        }
        polygons.push_back(CSGPolygon(vertices));
    }
    return CSG::fromPolygons(polygons);
}

CSGVertex sphere_vertex(double theta_i, double phi_i, glm::vec3 scale) {
    double theta = theta_i * M_PI * 2;
    double phi = phi_i * M_PI;
    glm::vec3 dir = glm::vec3(cos(theta) * sin(phi), cos(phi), sin(theta) * sin(phi));
    return CSGVertex(dir*scale, dir);
}

CSG CSGShapes::sphere(glm::vec3 scale, glm::vec3 rot, glm::vec3 center, int thetaRes, int phiRes) {
    std::vector<CSGPolygon> polygons;
    for (double i=0; i < thetaRes; i++) {
        for (double j=0; j < phiRes; j++) {
            std::vector<CSGVertex> vertices;
            vertices.push_back(sphere_vertex(i/thetaRes, j/phiRes, scale));
            if (j > 0) vertices.push_back(sphere_vertex((i+1)/thetaRes, j/phiRes, scale));
            if (j < phiRes-1) vertices.push_back(sphere_vertex((i+1)/thetaRes, (j+1)/phiRes, scale));
            vertices.push_back(sphere_vertex(i/thetaRes, (j+1)/phiRes, scale));
            polygons.push_back(CSGPolygon(vertices));
        }
    }
    return CSG::fromPolygons(polygons);
}

CSG CSGShapes::cylinder(glm::vec3 scale, glm::vec3 rot, glm::vec3 center, int thetaRes) {
    printf("Cylinder not implemented yet\n");
    assert(0);
    return CSG();
}
