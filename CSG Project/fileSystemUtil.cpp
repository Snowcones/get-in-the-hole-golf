//
//  fileSystemUtil.cpp
//  Demos
//
//  Created by William Henning on 8/19/15.
//  Copyright (c) 2015 William Henning. All rights reserved.
//

#include "fileSystemUtil.h"
#include <fstream>
#include <iostream>
#include <cstring>

/*
std::string getBundleFileSource(std::string name, std::string exten)
{
    char *bytes;
    CFStringRef nameCF, extenCF;
    bytes = (char *)CFAllocatorAllocate(CFAllocatorGetDefault(), name.size()+1, 0);
    strcpy(bytes, name.c_str());
    nameCF = CFStringCreateWithCStringNoCopy(NULL, bytes, kCFStringEncodingMacRoman, NULL);
    bytes = (char *)CFAllocatorAllocate(CFAllocatorGetDefault(), exten.size()+1, 0);
    strcpy(bytes, exten.c_str());
    extenCF = CFStringCreateWithCStringNoCopy(NULL, bytes, kCFStringEncodingMacRoman, NULL);
    CFURLRef fileURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), nameCF, extenCF, NULL);
    CFStringRef filePath = CFURLCopyFileSystemPath(fileURL, kCFURLPOSIXPathStyle);
    CFStringEncoding encodingMethod = CFStringGetSystemEncoding();
    const char *path = CFStringGetCStringPtr(filePath, encodingMethod);
    std::ifstream inFile(path);
    std::string sourceString=std::string((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());
    return sourceString;
}*/

std::string getFileSource(std::string name)
{
    std::ifstream infile(name);
    std::string str((std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());
    return str;
}
