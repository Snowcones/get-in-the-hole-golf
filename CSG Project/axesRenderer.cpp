//
//  axesRenderer.cpp
//  CSG Project
//
//  Created by William Henning on 5/20/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "axesRenderer.hpp"

int axesRenderer::screenWidth = 800;
int axesRenderer::screenHeight = 600;

glm::vec2 axesRenderer::orientation;
glm::vec3 axesRenderer::translation;
glm::vec4 axesRenderer::color;

int axesRenderer::verticesToDraw;
GLuint axesRenderer::vao;
GLuint axesRenderer::vbo;
GLuint axesRenderer::dataAttrib;
GLuint axesRenderer::axesProg;    //Could be static

GLuint axesRenderer::xColorUniform;
GLuint axesRenderer::yColorUniform;
GLuint axesRenderer::zColorUniform;

GLuint axesRenderer::modelMatUniform;
GLuint axesRenderer::viewMatUniform;
GLuint axesRenderer::projMatUniform;

glm::mat4 axesRenderer::modelMat;
glm::mat4 axesRenderer::viewMat;
glm::mat4 axesRenderer::projMat;

void axesRenderer::setup() {
    loadAndLinkShaders();
    setupBuffers();

    orientation = glm::vec2(0, 0);
    translation = glm::vec3(0, 0, -3);
    updateViewingMatrices();
    GetError();
}

void axesRenderer::loadAndLinkShaders() {
    GLuint vertexShader   = compileShaderFile(GL_VERTEX_SHADER, "bspDebug.vsh");
    GLuint fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, "bspDebug.fsh");

    if (0 != vertexShader && 0 != fragmentShader)
    {
        axesProg = glCreateProgram();

        glAttachShader(axesProg, vertexShader);
        glAttachShader(axesProg, fragmentShader);

        glBindFragDataLocation(axesProg, 0, "fragColor");
        linkProgram(axesProg);

        dataAttrib  = glGetAttribLocation(axesProg, "vertData");


        xColorUniform = glGetUniformLocation(axesProg, "frontColor");
        yColorUniform = glGetUniformLocation(axesProg, "thisColor");
        zColorUniform = glGetUniformLocation(axesProg, "backColor");
        viewMatUniform  = glGetUniformLocation(axesProg, "viewMat");
        modelMatUniform = glGetUniformLocation(axesProg, "modelMat");
        projMatUniform  = glGetUniformLocation(axesProg, "projMat");

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        glUseProgram(axesProg);
        glUniform4f(xColorUniform, 1, 0, 0, 1);
        glUniform4f(yColorUniform, 0, 1, 0, 1);
        glUniform4f(zColorUniform, 0, 0, 1, 1);
    }
    else
    {
        throw "Shader compilation failed";
    }
    GetError();
}

void axesRenderer::setupBuffers() {
    GetError();
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    int dataSize = 4 * 2 * 15;
    verticesToDraw = 2 * 15;
    float data[] = { 0, 0, 0, 0,     0, .5, 0, 0,
                     1, 0, 0, 0,     1, .5, 0, 0,
                    -1, 0, 0, 0,    -1, .5, 0, 0,
                     2, 0, 0, 0,     2, .5, 0, 0,
                    -2, 0, 0, 0,    -2, .5, 0, 0,


                    0,  0, 0, 1,     0,  0, .5, 1,
                    0,  1, 0, 1,     0,  1, .5, 1,
                    0, -1, 0, 1,     0, -1, .5, 1,
                    0,  2, 0, 1,     0,  2, .5, 1,
                    0, -2, 0, 1,     0, -2, .5, 1,

                    0, 0,  0, 2,     .5, 0,  0, 2,
                    0, 0,  1, 2,     .5, 0,  1, 2,
                    0, 0, -1, 2,     .5, 0, -1, 2,
                    0, 0,  2, 2,     .5, 0,  2, 2,
                    0, 0, -2, 2,     .5, 0, -2, 2};

    glBufferData(GL_ARRAY_BUFFER, dataSize * sizeof(float), data, GL_STATIC_DRAW);

    glEnableVertexAttribArray(dataAttrib);
    glVertexAttribPointer(dataAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
}

void axesRenderer::updateViewingMatrices() {
    modelMat = glm::mat4();
    modelMat = glm::eulerAngleYX(orientation.x, orientation.y);

    viewMat = glm::mat4();
    viewMat = glm::translate(viewMat, glm::vec3(translation));

    projMat = glm::perspective(3.1415f/2, (float)screenWidth/screenHeight, .1f, 100.0f);
}

void axesRenderer::setOrientation(glm::vec2 o) {
    orientation = o;
    updateViewingMatrices();
}

void axesRenderer::rotate(glm::vec2 r) {
    orientation += r;
    updateViewingMatrices();
}

void axesRenderer::setTranslation(glm::vec3 t) {
    translation = t;
    updateViewingMatrices();
}

void axesRenderer::translate(glm::vec3 t) {
    translation += t;
    updateViewingMatrices();
}

void axesRenderer::render() {
    glUseProgram(axesProg);
    glBindVertexArray(vao);

    glUniformMatrix4fv(modelMatUniform, 1, GL_FALSE, &modelMat[0][0]);
    glUniformMatrix4fv(viewMatUniform,  1, GL_FALSE, &viewMat[0][0]);
    glUniformMatrix4fv(projMatUniform,  1, GL_FALSE, &projMat[0][0]);
    GetError();

    glDrawArrays(GL_LINES, 0, verticesToDraw);
    GetError();
}
