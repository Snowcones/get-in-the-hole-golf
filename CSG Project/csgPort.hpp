//
//  csgPort.hpp
//  CSG Project
//
//  Created by William Henning on 6/12/16.
//  Copyright © 2016 William Henning. All rights reserved.
//
//  This code is my port of csg.js http://evanw.github.io/csg.js/docs/ to c++
//

#ifndef csgPort_hpp
#define csgPort_hpp

#include <vector>
#include <glm/glm.hpp>
#include "csgPortGeometry.hpp"

#include "csgVertex.hpp"
#include "csgPlane.hpp"
#include "csgPolygon.hpp"
#include "csgNode.hpp"

//I'm ommiting the shared member from the CSGPolygon class for now at least. I don't have a need for it and am not sure how to implement it.
//Otherwise this code is a fairly faithful port of csg.js.
//I don't think the calls to .clone() in my code are necessary.

glm::vec3 lerp();

class CSGPlane;
class CSGPolygon;

class CSG {
public:
    std::vector<CSGPolygon> polygons;
    
    CSG();
    CSG (std::vector<CSGPolygon> inPolygons);
    CSG static fromPolygons(std::vector<CSGPolygon> inPolygons);
    CSG clone();
    std::vector<CSGPolygon> toPolygons();
    CSG unionOf(CSG csg);
    CSG subtract(CSG csg);
    CSG intersect(CSG csg);
    CSG inverse();
};
#endif /* csgPort_hpp */
