//
//  bspRenderer.cpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "bspRenderer.hpp"
#include "csgTesting.hpp"

glm::vec3 pointCloud::generatePoint() {
    if (shape == CLOUD_CUBE)
    {
        return glm::vec3( 2.0*x * ((float)rand()/RAND_MAX - .5), 2.0*y * ((float)rand()/RAND_MAX - .5), 2.0*z * ((float)rand()/RAND_MAX - .5) );
    }
    else if (shape == CLOUD_SPHERE)
    {
        float distSquared = r*r + .1;
        glm::vec3 p;
        while (distSquared > r*r)
        {
            p = glm::vec3( 2.0*r * ((float)rand()/RAND_MAX - .5), 2.0*r * ((float)rand()/RAND_MAX - .5), 2.0*r * ((float)rand()/RAND_MAX - .5) );
            distSquared = glm::dot(p, p);
        }
        return p;
    }
    else
    {
        assert(0);
    }
}

bspRenderer::bspRenderer() {
    loadAndLinkShaders();
    setupBuffers();
    setColors(glm::vec4(1.0), glm::vec4(.1));
}

void bspRenderer::setupBuffers() {
    
    GetError();
    glGenVertexArrays(1, &testVao);
    glBindVertexArray(testVao);
    glGenBuffers(1, &testVbo);
    glBindBuffer(GL_ARRAY_BUFFER, testVbo);
    GetError();
    
    glEnableVertexAttribArray(vertAttrib);
    GetError();
    glVertexAttribPointer(vertAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    GetError();
}

void bspRenderer::bufferBSPTree(bspTreeNode *t) {
    float* data;
    int numTriangles;
    
    t->getTriangleList(&numTriangles, &data);
    bufferTriList(numTriangles, data);
    
    free(data);
    GetError();
}

void bspRenderer::bufferBSPTree(CSG t) {
    float* data;
    int numTriangles;
    
    getTriangleList(t, &numTriangles, &data);
    bufferTriList(numTriangles, data);
    printf("CSGPort Num Buffered: %d\n", numTriangles);
    
    free(data);
    GetError();
}

void bspRenderer::bufferVolumeTest()
{
    if (hasVolumeTest && false)
    {
        return;
    }
    else
    {
        int numPoints = (int)pointsTested.size();
        float testData[4*numPoints];
        for (int i=0; i<numPoints; i++)
        {
            testData[4*i] = pointsTested[i].x;
            testData[4*i+1] = pointsTested[i].y;
            testData[4*i+2] = pointsTested[i].z;
            testData[4*i+3] = pointStatus[i]*1.0;
        }
        
        GetError();
        glBindVertexArray(testVao);
        glBindBuffer(GL_ARRAY_BUFFER, testVbo);
        glBufferData(GL_ARRAY_BUFFER, numPoints*4*sizeof(float), testData, GL_STATIC_DRAW);
        glEnableVertexAttribArray(vertAttrib);
        glVertexAttribPointer(vertAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
        GetError();
        
        volumePoints = numPoints;
    }
}

void bspRenderer::setupVolumeTest(pointCloud c, int numPoints, bspTreeNode* n){
    
    pointsTested.clear();
    pointStatus.clear();
    for (int i=0; i<numPoints; i++)
    {
        pointsTested.push_back(c.generatePoint());
        pointStatus.push_back(pointIsInBSPTree(pointsTested.back(), n));
    }
    
    bufferVolumeTest();
    hasVolumeTest = true;
    isWireFrame = true;
    GetError();
}

void bspRenderer::setupVolumeTest(pointCloud c, int numPoints, CSG t){
    CSGNode n = CSGNode(t.toPolygons());
    pointsTested.clear();
    pointStatus.clear();
    for (int i=0; i<numPoints; i++)
    {
        pointsTested.push_back(c.generatePoint());
        pointStatus.push_back(pointIsInBSPTree(pointsTested.back(), n));
    }
    
    bufferVolumeTest();
    hasVolumeTest = true;
    isWireFrame = true;
    GetError();
}

void bspRenderer::renderOpaque() {
    isWireFrame = false;
}

void bspRenderer::render() {
    
    //Draw shape
    GetError();
    glUseProgram(prog);
    glBindVertexArray(vao);
    
    glUniform4fv(colorUniform, 1, &color[0]);
    glUniformMatrix4fv(modelMatUniform, 1, GL_FALSE, &modelMat[0][0]);
    glUniformMatrix4fv(viewMatUniform,  1, GL_FALSE, &viewMat[0][0]);
    glUniformMatrix4fv(projMatUniform,  1, GL_FALSE, &projMat[0][0]);
//    printf("%d, %d, %d, %d, %d\n", prog, colorUniform, modelMatUniform, viewMatUniform, projMatUniform);
    GetError();
    
    if (isWireFrame)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawArrays(GL_TRIANGLES, 0, verticesToDraw);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDrawArrays(GL_TRIANGLES, 0, verticesToDraw);
    }
    
    
    //Draw volume test
    if (hasVolumeTest && isWireFrame)
    {
        GetError();
        glUseProgram(solidProg);
        glBindVertexArray(testVao);
        GetError();
        
        glUniform4fv(volumeInColor, 1, &inColor[0]);
        glUniform4fv(volumeOutColor, 1, &outColor[0]);
        glUniformMatrix4fv(testModelUniform, 1, GL_FALSE, &modelMat[0][0]);
        glUniformMatrix4fv(testViewUniform,  1, GL_FALSE, &viewMat[0][0]);
        glUniformMatrix4fv(testProjUniform,  1, GL_FALSE, &projMat[0][0]);
        GetError();
        
        
        glDrawArrays(GL_POINTS, 0, volumePoints);
        GetError();
    }
    GetError();
}

void bspRenderer::setColors(glm::vec4 inC, glm::vec4 outC) {
    
    inColor = inC;
    outColor = outC;
    glUseProgram(solidProg);
    glUniform4fv(volumeInColor, 1, &inColor[0]);
    glUniform4fv(volumeOutColor, 1, &outColor[0]);
    
    GetError();
}

void bspRenderer::loadAndLinkShaders() {
    
    GLuint vertexShader   = compileShaderFile(GL_VERTEX_SHADER, "solidColor.vsh");
    GLuint fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, "solidColor.fsh");
    
    if (0 != vertexShader && 0 != fragmentShader)
    {
        solidProg = glCreateProgram();
        
        glAttachShader(solidProg, vertexShader);
        glAttachShader(solidProg, fragmentShader);
        
        glBindFragDataLocation(solidProg, 0, "fragColor");
        linkProgram(solidProg);
        
        vertAttrib = glGetAttribLocation(solidProg, "vertData");
        
        volumeInColor = glGetUniformLocation(solidProg, "inColor");
        volumeOutColor = glGetUniformLocation(solidProg, "outColor");
        
        testViewUniform  = glGetUniformLocation(solidProg, "viewMat");
        testModelUniform = glGetUniformLocation(solidProg, "modelMat");
        testProjUniform  = glGetUniformLocation(solidProg, "projMat");
        
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }
    else
    {
        throw "Shader compilation failed";
    }
    GetError();
}