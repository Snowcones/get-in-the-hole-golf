//
//  csgPortGeometry.hpp
//  CSG Project
//
//  This code is part of my port of csg.js http://evanw.github.io/csg.js/docs/ to c++
//

#ifndef csgPortGeometry_h
#define csgPortGeometry_h

class CSG;
class CSGShapes
{
public:
    // Code for geometry
    CSG static cube(glm::vec3 scale = glm::vec3(1), glm::vec3 rot = glm::vec3(0), glm::vec3 center = glm::vec3(0));
    CSG static sphere(glm::vec3 scale = glm::vec3(1), glm::vec3 rot = glm::vec3(0), glm::vec3 center = glm::vec3(0), int thetaRes = 20, int phiRes = 10);
    CSG static cylinder(glm::vec3 scale = glm::vec3(1), glm::vec3 rot = glm::vec3(0), glm::vec3 center = glm::vec3(0), int thetaRes = 20);
};


#endif /* csgPortGeometry_h */
