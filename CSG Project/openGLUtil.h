//Stolen Code : (

#ifndef __Demos__openGLUtil__
#define __Demos__openGLUtil__

#include <stdio.h>
#include <string>
#include <vector>
#include "error.h"
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>


struct shaderOutline {
    std::string vertShader;
    std::string fragShader;

    std::vector<GLuint> fragDataLocations;
    std::vector<std::string> fragDataNames;

    std::vector<GLuint*> attribLocations;
    std::vector<std::string> attribNames;

    std::vector<GLuint*> uniformLocations;
    std::vector<std::string> uniformNames;
};

GLuint compileShaderFile(GLenum type, std::string name);
GLuint compileShaderSource(GLenum type, GLchar const* source, std::string name);
GLuint compileShader(shaderOutline outline);

void linkProgram(GLuint program);
void validateProgram(GLuint program);
#endif /* defined(__Demos__openGLUtil__) */
