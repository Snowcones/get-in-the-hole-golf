//
//  bspStepThroughRenderer.cpp
//  CSG Project
//
//  Created by William Henning on 5/19/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "bspStepThroughRenderer.hpp"
#include <algorithm>

bspTestRenderer::bspTestRenderer() {
    setupBuffers();
    loadAndLinkShaders();
    setColors(glm::vec4(0, 1, 0, 1), glm::vec4(0, 0, 1, 1), glm::vec4(1, 0, 0, 1));
}

void bspTestRenderer::setupBuffers() {

    glGenVertexArrays(1, &nVao);
    glBindVertexArray(nVao);
    glGenBuffers(1, &nVbo);
    glBindBuffer(GL_ARRAY_BUFFER, nVbo);

    glEnableVertexAttribArray(nAttrib);
    glVertexAttribPointer(nAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
}

void bspTestRenderer::setTree(bspTreeNode *t) {
    currentNode = t;
    treeLocation.clear();
    bufferTree();
}

void bspTestRenderer::moveToBack() {
    if (currentNode->backSubtree)
    {
        testTrees(currentNode, currentNode->backSubtree);
        treeLocation.push_back(currentNode);
        currentNode = currentNode->backSubtree;
        bufferTree();
    }
}

void bspTestRenderer::moveToFront() {
    if (currentNode->frontSubtree)
    {
        testTrees(currentNode, currentNode->frontSubtree);
        treeLocation.push_back(currentNode);
        currentNode = currentNode->frontSubtree;
        bufferTree();
    }
}

void bspTestRenderer::moveUp() {
    if (treeLocation.size() == 0)
    {
        return;
    }
    currentNode = treeLocation.back();
    treeLocation.pop_back();
    bufferTree();
}

void bspTestRenderer::bufferTree() {
    if (currentNode->triNode.n == glm::vec3(0))
    {
        printf("Buffered degenerate tree\n");
        return;
    }

    //testTrees(currentNode, currentNode->backSubtree);

    //Buffer Norms
    std::vector<triangle> triangles;
    currentNode->getTriangleList(triangles);


    //triangles.erase(triangles.begin());

    normalPoints = 6 * (int)triangles.size();

    float normData[24 * triangles.size()];
    for (int i=0; i<triangles.size(); i++)
    {
        const float nm = .1;

        //Put the endpoints of our debug normals into the normData array
        glm::vec3 currentVec = triangles[i].a;
        normData[24*i]     = currentVec.x;
        normData[24*i + 1] = currentVec.y;
        normData[24*i + 2] = currentVec.z;
        normData[24*i + 3] = 0.0;

        currentVec = triangles[i].a + triangles[i].n * nm;
        normData[24*i + 4] = currentVec.x;
        normData[24*i + 5] = currentVec.y;
        normData[24*i + 6] = currentVec.z;
        normData[24*i + 7] = 0.0;

        currentVec = triangles[i].b;
        normData[24*i + 8]  = currentVec.x;
        normData[24*i + 9]  = currentVec.y;
        normData[24*i + 10] = currentVec.z;
        normData[24*i + 11] = 0.0;

        currentVec = triangles[i].b + triangles[i].n * nm;
        normData[24*i + 12] = currentVec.x;
        normData[24*i + 13] = currentVec.y;
        normData[24*i + 14] = currentVec.z;
        normData[24*i + 15] = 0.0;

        currentVec = triangles[i].c;
        normData[24*i + 16] = currentVec.x;
        normData[24*i + 17] = currentVec.y;
        normData[24*i + 18] = currentVec.z;
        normData[24*i + 19] = 0.0;

        currentVec = triangles[i].c + triangles[i].n * nm;
        normData[24*i + 20] = currentVec.x;
        normData[24*i + 21] = currentVec.y;
        normData[24*i + 22] = currentVec.z;
        normData[24*i + 23] = 0.0;
    }

    glBindVertexArray(nVao);
    glBindBuffer(GL_ARRAY_BUFFER, nVbo);
    glBufferData(GL_ARRAY_BUFFER, 24 * triangles.size() * sizeof(float), normData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(nAttrib);
    glVertexAttribPointer(nAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //return;                         //////////////////////////Turns off faces
    //Buffer Triangles
    triangles.clear();
    triangles.push_back(currentNode->triNode);
    int frontLength = 0;
    if (currentNode->frontSubtree)
    {
        currentNode->frontSubtree->getTriangleList(triangles);
        frontLength = (int)triangles.size() - 1;
    }
    if (currentNode->backSubtree)
    {
        currentNode->backSubtree->getTriangleList(triangles);
    }

    int totalTris = (int)triangles.size();
    verticesToDraw = 3 * totalTris;
    float posData[4 * 3 * totalTris];

    float z = 1.0;
    for (int i=0; i<triangles.size(); i++)
    {
        if (i >= 1 && i <= frontLength)
        {
            z = 0.0;
        }
        else if (i > frontLength)
        {
            z = 2.0;
        }

        glm::vec3 currentVec = triangles[i].a;
        posData[12*i]     = currentVec.x;
        posData[12*i + 1] = currentVec.y;
        posData[12*i + 2] = currentVec.z;
        posData[12*i + 3] = z;

        currentVec = triangles[i].b;
        posData[12*i + 4] = currentVec.x;
        posData[12*i + 5] = currentVec.y;
        posData[12*i + 6] = currentVec.z;
        posData[12*i + 7] = z;

        currentVec = triangles[i].c;
        posData[12*i + 8]  = currentVec.x;
        posData[12*i + 9]  = currentVec.y;
        posData[12*i + 10] = currentVec.z;
        posData[12*i + 11] = z;
    }
    printf("Buffered debug tree has %d vertices\n", verticesToDraw);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, totalTris * 12 * sizeof(float), posData, GL_STATIC_DRAW);
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    //glDisableVertexAttribArray(normAttrib);
}

void bspTestRenderer::setColors(glm::vec4 frontC, glm::vec4 thisC, glm::vec4 backC) {
    frontColor = frontC;
    thisColor = thisC;
    backColor = backC;
}

void bspTestRenderer::render() {
    //Draw shape
    GetError();
    glUseProgram(prog);
    glBindVertexArray(nVao);
    //printf("Solid renderering: %d\n", vao);
    GetError();


    glUniformMatrix4fv(nModelMatUniform, 1, GL_FALSE, &modelMat[0][0]);
    GetError();
    glUniformMatrix4fv(nViewMatUniform,  1, GL_FALSE, &viewMat[0][0]);
    GetError();
    glUniformMatrix4fv(nProjMatUniform,  1, GL_FALSE, &projMat[0][0]);
    GetError();

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    if (gNormals) glDrawArrays(GL_LINES, 0, normalPoints);

    GetError();
    glUseProgram(debugProg);
    glBindVertexArray(vao);
    GetError();

    glUniform4fv(frontColorUniform, 1, &frontColor[0]);
    glUniform4fv(thisColorUniform,  1, &thisColor[0]);
    glUniform4fv(backColorUniform,  1, &backColor[0]);
    glUniformMatrix4fv(dModelUniform, 1, GL_FALSE, &modelMat[0][0]);
    glUniformMatrix4fv(dViewUniform,  1, GL_FALSE, &viewMat[0][0]);
    glUniformMatrix4fv(dProjUniform,  1, GL_FALSE, &projMat[0][0]);
    GetError();

    if (gWireframe) {glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);}
    else {glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);}

    glDrawArrays(GL_TRIANGLES, 0, verticesToDraw);
    GetError();
}

void bspTestRenderer::loadAndLinkShaders() {

    //Normal Renderer
    GLuint vertexShader   = compileShaderFile(GL_VERTEX_SHADER, "white.vsh");
    GLuint fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, "white.fsh");

    GLuint* thisProg = &normalProg;
    if (0 != vertexShader && 0 != fragmentShader)
    {
        *thisProg = glCreateProgram();

        glAttachShader(*thisProg, vertexShader);
        glAttachShader(*thisProg, fragmentShader);

        glBindFragDataLocation(*thisProg, 0, "fragColor");
        linkProgram(*thisProg);
        GetError();

        nAttrib  = glGetAttribLocation(*thisProg, "vertData");
        GetError();

        nViewMatUniform  = glGetUniformLocation(*thisProg, "viewMat");
        nModelMatUniform = glGetUniformLocation(*thisProg, "modelMat");
        nProjMatUniform  = glGetUniformLocation(*thisProg, "projMat");

        GetError();

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }
    else
    {
        throw "Shader compilation failed";
    }
    GetError();


    //Face Renderer
    vertexShader   = compileShaderFile(GL_VERTEX_SHADER, "bspDebug.vsh");
    fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, "bspDebug.fsh");

    thisProg = &debugProg;
    if (0 != vertexShader && 0 != fragmentShader)
    {
        *thisProg = glCreateProgram();

        glAttachShader(*thisProg, vertexShader);
        glAttachShader(*thisProg, fragmentShader);

        glBindFragDataLocation(*thisProg, 0, "fragColor");
        linkProgram(*thisProg);

        GetError();
        posAttrib  = glGetAttribLocation(*thisProg, "vertData");
        GetError();
        frontColorUniform = glGetUniformLocation(*thisProg, "frontColor");
        thisColorUniform  = glGetUniformLocation(*thisProg, "thisColor");
        backColorUniform  = glGetUniformLocation(*thisProg, "backColor");
        dViewUniform  = glGetUniformLocation(*thisProg, "viewMat");
        dModelUniform = glGetUniformLocation(*thisProg, "modelMat");
        dProjUniform  = glGetUniformLocation(*thisProg, "projMat");
        GetError();
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        GetError();
    }
    else
    {
        throw "Shader compilation failed";
    }
    GetError();
}


void bspTestRenderer::testTrees(bspTreeNode *t1, bspTreeNode *t2) {
    typedef std::pair<glm::vec3, glm::vec3> absNorm;
    std::vector<triangle> tris1;
    std::vector<absNorm> norms1;
    std::vector<triangle> tris2;
    std::vector<absNorm> norms2;


    t1->getTriangleList(tris1);
    t2->getTriangleList(tris2);

    for (triangle t : tris1)
    {
        norms1.push_back(absNorm(t.a, t.n));
    }
    for (triangle t : tris2)
    {
        norms2.push_back(absNorm(t.a, t.n));
    }

    for (absNorm an : norms2)
    {
        auto it = std::find(norms1.begin(), norms1.end(), an);
        if (it == norms1.end())
        {
            printf("What!\n");
        }
    }
}
