//
//  csg.hpp
//  CSG Project
//
//  Created by William Henning on 5/5/16.
//  Copyright © 2016 William Henning. All rights reserved.
//
//  References:
//  evanw.github.io/csg.js/docs
//

#ifndef csg_hpp
#define csg_hpp

#include <stdio.h>
#include <vector>
#include <math.h>
#include "glm/glm.hpp"
#include "triangle.hpp"

double const K_NULL_TRI_DIST  = -10e35;
double const K_NULL_TRI_BOUND = -10e34;

typedef enum {
    CSG_OR,
    CSG_AND,
    CSG_NEGATE,
} csgOperator;



class bspTreeNode {
private:
    void uncheckedGetTriList(std::vector<triangle>& list);
    void uncheckedInvert();
    void copyNode(bspTreeNode* n);
    void removeDegenerateFromNode(bspTreeNode* n);
    
public:
    triangle triNode;
    bspTreeNode* backSubtree;
    bspTreeNode* frontSubtree;
    
    bspTreeNode();
    bspTreeNode(const bspTreeNode& t);
    bspTreeNode(const triangle& t);
    
    //Basic methods
    void add(triangle t);
    void insert(triangle t, std::vector<triangle>& outsideTriangles);
//    void insert(triangle t, std::vector<triangle>& outsideTriangles, bool coplanar);
    void getTriangleList(std::vector<triangle>& list);
    void getTriangleList(int* trianglesToDraw, float **data);
    void clipTo(bspTreeNode* clippingTree, std::vector<triangle>& outsideTriangles);
//    void clipTo(bspTreeNode* clippingTree, std::vector<triangle>& outsideTriangles, bool coplanar);
    void clipList(std::vector<triangle>& list);
//    void clipList(std::vector<triangle>& list, bool coplanar);
    
    
    void translate(glm::vec3 t);
    void scale(glm::vec3 s);
    void rotate(glm::vec3 r);
    bool raytrace(glm::vec3 origin, glm::vec3 direction, glm::vec3& intersection);
    
    void invert();
    float dist(glm::vec3 p);
    bool isPositiveDegenerate();
    bool isNegativeDegenerate();
    void removeDegenerateFromTree();
    //void removeCoplanar(std::vector<triangle>& tris);
    
    //Boolean CSG methods
    bspTreeNode* andCSG(bspTreeNode *clippingTree);
    static bspTreeNode* andCSG(bspTreeNode* tree1, bspTreeNode* tree2);
    bspTreeNode* orCSG(bspTreeNode *clippingTree);
    static bspTreeNode* orCSG(bspTreeNode* tree1, bspTreeNode* tree2);
    bspTreeNode* subCSG(bspTreeNode *clippingTree);
    static bspTreeNode* subCSG(bspTreeNode* tree1, bspTreeNode* tree2);
};

void splitTris(triangle t, triangle splitter, std::vector<triangle>& front, std::vector<triangle>& back);

#endif /* csg_hpp */
