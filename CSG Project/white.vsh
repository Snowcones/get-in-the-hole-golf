#version 330

in vec4 vertData;

uniform mat4 modelMat;
uniform mat4 projMat;
uniform mat4 viewMat;

void main()
{
    gl_Position = projMat * viewMat * modelMat * vec4(vertData.xyz, 1.0);
}