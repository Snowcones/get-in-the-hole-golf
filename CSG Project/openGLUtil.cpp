#include "openGLUtil.h"
#include "fileSystemUtil.h"

GLuint compileShaderFile(GLenum type, std::string name)
{
    GetError();
    std::string source;
    std::size_t extenLoc=name.find_last_of(".");
    std::string exten=name.substr(extenLoc+1);
    //source=getBundleFileSource(name, exten);
    source=getFileSource(name);
    return compileShaderSource(type, source.c_str(), name);
}


GLuint compileShaderSource(GLenum type, GLchar const* source, std::string name)
{
    GetError();
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    GetError();
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar*)malloc((size_t)logLength);
        glGetShaderInfoLog(shader, logLength, &logLength, log);
        printf("Shader compilation failed with error:\n%s", log);
        free(log);
    }
#endif
    
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (0 == status)
    {
        glDeleteShader(shader);
        printf("Shader compilation failed for file %s\n", name.c_str());
    }
    return shader;
    GetError();
}

void linkProgram(GLuint program)
{
    GetError();
    glLinkProgram(program);
    GetError();
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar*)malloc((size_t)logLength);
        glGetProgramInfoLog(program, logLength, &logLength, log);
        GetError();
        printf("Shader program linking failed with error:\n%s", log);
        free(log);
    }
#endif
    
    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status==0)
    {
        printf("Shader linking failed\n");
    }
    GetError();
}

void validateProgram(GLuint program)
{
    GetError();
    GLint logLength;
    glValidateProgram(program);
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
    GetError();
    if (logLength > 0)
    {
        GLchar *log = (GLchar*)malloc((size_t)logLength);
        glGetProgramInfoLog(program, logLength, &logLength, log);
        printf("Program validation produced errors:\n%s", log);
        free(log);
    }
    
    GLint status;
    glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
    if (0 == status)
    {
        throw "Failed to link shader program";
        //[NSException raise:kFailedToInitialiseGLException format:@"Failed to link shader program"];
    }
    GetError();
}


//I'm not sure the following actually improves anything
GLuint compileShader(shaderOutline outline) {
    GLuint vertexShader   = compileShaderFile(GL_VERTEX_SHADER, outline.vertShader);
    GLuint fragmentShader = compileShaderFile(GL_FRAGMENT_SHADER, outline.fragShader);
    
    GLuint program;
    
    if (0 != vertexShader && 0 != fragmentShader)
    {
        program = glCreateProgram();
        
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        
        for (int i=0; i<outline.fragDataLocations.size(); i++)
        {
            glBindFragDataLocation(program, outline.fragDataLocations[i], outline.fragDataNames[i].c_str());
        }
        
        linkProgram(program);
        
        for (int i=0; i<outline.attribLocations.size(); i++)
        {
            *(outline.attribLocations[i]) = glGetAttribLocation(program, outline.attribNames[i].c_str());
        }
        
        for (int i=0; i<outline.attribLocations.size(); i++)
        {
            *(outline.uniformLocations[i]) = glGetUniformLocation(program, outline.uniformNames[i].c_str());
        }
        
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }
    else
    {
        throw "Shader compilation failed";
    }
    
    GetError();
    return program;
}