//
//  bspRenderer.hpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#ifndef bspRenderer_hpp
#define bspRenderer_hpp

//Stuff needed
#include <stdio.h>
#include "csg.hpp"
#include "csgPort.hpp"
#include "csgPortRenderUtility.hpp"
#include "openGLUtil.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

//Renderers
#include "diffuseRenderer.hpp"

typedef enum {
    CLOUD_CUBE,
    CLOUD_SPHERE,
} pointCloudShape;

struct pointCloud {
    pointCloudShape shape;
    union {float r; float x;};
    float y;
    float z;

    glm::vec3 generatePoint();
};

class bspRenderer: public diffuseRenderer
{
private:

    int volumePoints = 0;
    bool hasVolumeTest = false;
    bool isWireFrame = true;

    glm::vec4 inColor;
    glm::vec4 outColor;
    std::vector<glm::vec3> pointsTested;
    std::vector<bool> pointStatus;

    GLuint testVao;
    GLuint testVbo;
    GLuint solidProg;
    GLuint vertAttrib;

    GLuint volumeInColor;
    GLuint volumeOutColor;

    GLuint testModelUniform;
    GLuint testViewUniform;
    GLuint testProjUniform;

    void bufferVolumeTest();
    void loadAndLinkShaders();
    void setupBuffers();

public:
    bspRenderer();
    void render();

    void bufferBSPTree(bspTreeNode* t); //One of these should go
    void bufferBSPTree(CSG t);

    void setupVolumeTest(pointCloud p, int numPoints, bspTreeNode* n);
    void setupVolumeTest(pointCloud c, int numPoints, CSG t);
    void renderOpaque();
    void setColors(glm::vec4 inC, glm::vec4 outC);
};

#endif /* bspRenderer_hpp */
