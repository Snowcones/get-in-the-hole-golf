#version 330

in vec4 drawColor;
out vec4 fragColor;

void main()
{
    if (drawColor.a < .01)
    {
        discard;
    }
    fragColor = drawColor;
}