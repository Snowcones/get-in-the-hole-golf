//
//  triangle.cpp
//  CSG Project
//
//  Created by William Henning on 5/17/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "triangle.hpp"

bool triangle::operator==(const triangle &other) const {
    return a == other.a && b == other.b && c == other.c;
}

bool triangle::operator!=(const triangle &other) const {
    return !(*this == other);
}

triangle::triangle() {
    a = glm::vec3(0);
    b = glm::vec3(0);
    c = glm::vec3(0);
    n = glm::vec3(0);
    d = 0;
}

triangle::triangle(glm::vec3 inA, glm::vec3 inB, glm::vec3 inC) {
    a = inA;
    b = inB;
    c = inC;
    this->recalculateNorm();
    this->recalculateD();
    A = glm::length(glm::cross(b-a, c-a));
    
    
    #ifdef DEBUG
    if (n!=n)
    {
        printf("Normal is Nan\n");
    }
    
    debugHash = hashTri(*this);
    #endif
}

float triangle::dist(glm::vec3 p) {
    return glm::dot(p, n) + d;
}

glm::vec3 triangle::calculateIntersection(glm::vec3 p1, glm::vec3 p2) {
    float t = -(glm::dot(n, p1) + d)/(glm::dot(n, p2 - p1));
    return p1 + t*(p2 - p1);
}

void triangle::recalculateNorm() {
    glm::vec3 d1 = b - a;
    glm::vec3 d2 = c - a;
    n = glm::normalize(glm::cross(d1, d2));
}

void triangle::recalculateD() {
    d = - glm::dot(a, n);
}

//Möller-Trumbore intersection algorithm
bool triangle::rayIntersection(glm::vec3 origin, glm::vec3 direction, glm::vec3& intersection) {
    const float rEpsilon = 1e-5;
    glm::vec3 e1, e2;
    glm::vec3 P, Q, T;
    float det, inv_det, u, v, t;
    
    e1 = b-a;
    e2 = c-a;
    
    P = glm::cross(direction, e2);
    det = glm::dot(e1, P);
    if (det > -rEpsilon && det < rEpsilon)
    {
        return false;
    }
    inv_det = 1/det;
    T = origin - a;
    u = glm::dot(T, P) * inv_det;
    if (u < 0 || u > 1)
    {
        return false;
    }
    
    Q = glm::cross(T, e1);
    v = glm::dot(direction, Q) * inv_det;
    if (v < 0 || u + v > 1)
    {
        return false;
    }
    
    t = glm::dot(e2, Q) * inv_det;
    if (t > rEpsilon)
    {
        intersection = origin + t * direction;
        return true;
    }
    
    return false;
}

void calculateIntersectionPoints(triangle splitter, triangle splittee, glm::vec3& A, glm::vec3& B) {
    glm::vec3 a = splittee.a;
    glm::vec3 b = splittee.b;
    glm::vec3 c = splittee.c;
    
    glm::vec3 n = splitter.n;
    float d = splitter.d;
    
    #ifdef DEBUG
    if(glm::dot(n, c - a) == 0)
    {
        printf("Triangle intersection called with parrellel plane and triangle\n");
        printf("Will be Nan\n");
    }
    #endif
    
    float t1 = -(glm::dot(n, a) + d)/(glm::dot(n, c - a));
    float t2 = -(glm::dot(n, b) + d)/(glm::dot(n, c - b));
    
    A = a + t1*(c - a);
    B = b + t2*(c - b);
}

void calculateSingleIntersection(triangle splitter, glm::vec3 e1, glm::vec3 e2, glm::vec3& P) {
    glm::vec3 n = splitter.n;
    float d = splitter.d;
    
    float t = -(glm::dot(n, e1) + d)/(glm::dot(n, e2 - e1));
    P = e1 + t*(e2 - e1);
}