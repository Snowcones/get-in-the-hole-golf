//
//  csgPlane.cpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include <stdio.h>

#include "csgPlane.hpp"
#include "csgVertex.hpp"
#include "csgPolygon.hpp"

#ifdef DEBUG
//I added this method myself. It should check if a set of CSGVertices forms a convex polygon. I haven't tested this code yet.
bool isConvex(std::vector<CSGVertex> vertices)
{
    glm::vec3 n = glm::cross(vertices[2].pos - vertices[0].pos, vertices[1].pos - vertices[0].pos);
    int points = (int)vertices.size();
    for (int i=0; i<points; i++)
    {
        glm::vec3 thisN = glm::cross(vertices[(2+i)%points].pos - vertices[i].pos, vertices[(1+i)%points].pos - vertices[i].pos);
        if (glm::dot(n, thisN) < 0)
        {
            return false;
        }
    }
    return true;
}
#endif

CSGPlane::CSGPlane(glm::vec3 inNormal, float inW) {
    normal = inNormal;
    w = inW;
}

//////////////////// Helper functions for the CSGPlane(std::vector<CSGVertex> verts) constructor
float area(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
    return glm::length(glm::cross(b-a, c-a));
}

float area(const std::vector<CSGVertex>& verts, int a, int b, int c)
{
    return area(verts[a].pos, verts[b].pos, verts[c].pos);
}
////////////////////

CSGPlane::CSGPlane(std::vector<CSGVertex> verts) {

//    printf("Ran fancy\n");
#ifdef DEBUG
    if (!isConvex(verts))
    {
        printf("Nonconvex Poly!\n");
    }
#endif

    //http://stackoverflow.com/questions/1621364 this algorithm finds the largest triangle in our polygon to generate our normals
    int a = 0, b = 1, c = 2;
    int bA = a, bB = b, bC = c;
    int n = (int)verts.size();
    int cLock;
    int bLock = 0;
    while (true)
    {
        while (true)
        {
            cLock = c;
            while (area(verts, a, b, c) <= area(verts, a, b, (c+1)%n))
            {
                c = (c+1)%n;
                if (c == cLock) //Code for bad polys
                {
                    b = (b+1)%n;
                    c = (c+1)%n;
                    break;
                }
            }
            if (area(verts, a, b, c) <= area(verts, a, (b+1)%n, c))
            {
                bLock++;
                if (bLock>2*n)  //Code for bad polys
                {
                    break;
                }
                b = (b+1)%n;
                continue;
            }
            else
            {
                bLock = 0;
                break;
            }
        }

        if (area(verts, a, b, c) > area(verts, bA, bB, bC))
        {
            bA = a;
            bB = b;
            bC = c;
        }

        a = (a+1)%n;
        if (a == b)
        {
            b = (b+1)%n;
        }
        if (b == c)
        {
            c = (c+1)%n;
        }
        if (a == 0)
        {
            break;
        }
    }

    //Double precision here greatly improves accuracy of CSG
    glm::dvec3 aVec = verts[bA].pos;
    glm::dvec3 bVec = verts[bB].pos;
    glm::dvec3 cVec = verts[bC].pos;

    glm::dvec3 dNorm = glm::normalize(glm::cross(bVec - aVec, cVec - aVec));
    normal = dNorm;
    w = glm::dot(dNorm, aVec);
}

CSGPlane::CSGPlane(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
    printf("Warning, glitched plane constructor called\n"); //I should probably remove this constructor altogether at some point
    normal = glm::normalize(glm::cross(b - a, c - a));
    w = glm::dot(normal, a);
}

CSGPlane CSGPlane::fromPoints(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
    return CSGPlane(a, b, c);
}

CSGPlane CSGPlane::clone() {
    return CSGPlane(normal, w);
}

CSGPlane* CSGPlane::duplicate() {
    return new CSGPlane(normal, w);
}

void CSGPlane::flip() {
    normal *= -1;
    w *= -1;
}

void CSGPlane::splitPolygon(CSGPolygon& polygon, std::vector<CSGPolygon> &coplanarFront, std::vector<CSGPolygon> &coplanarBack, std::vector<CSGPolygon> &front, std::vector<CSGPolygon> &back) {
    const int COPLANAR = 0; //Because of bithacking below coplanar must be 0, spanning must be 3, and front and back must be 1 and 2
    const int FRONT    = 1;
    const int BACK     = 2;
    const int SPANNING = 3;


    int polygonType = 0;
    std::vector<int> types;
    for (int i = 0; i<polygon.vertices.size(); i++)
    {
        float t = glm::dot(normal, polygon.vertices[i].pos) - w; //The distance of the ith vertex
        int type = (t < -CSGPlane::EPSILON) ? BACK : (t > CSGPlane::EPSILON) ? FRONT : COPLANAR; //Classifies vertex as front back or coplanar based on distance
        polygonType |= type; //Classifies face based on past vertices
        types.push_back(type);
    }

    switch (polygonType) {
        case COPLANAR:
        {
            (glm::dot(normal, polygon.plane.normal) > 0 ? coplanarFront : coplanarBack).push_back(polygon);
            break;
        }

        case FRONT:
        {
            front.push_back(polygon);
            break;
        }

        case BACK:
        {
            back.push_back(polygon);
            break;
        }

        case SPANNING:
        {
            std::vector<CSGVertex> f; //Tracks front vertices
            std::vector<CSGVertex> b; //Tracks back vertices
            for (int i = 0; i < polygon.vertices.size(); i++)
            {
                int j = (i + 1) % polygon.vertices.size();
                int ti = types[i];
                int tj = types[j];
                CSGVertex vi = polygon.vertices[i];
                CSGVertex vj = polygon.vertices[j];
                if (ti != BACK)  f.push_back(vi);
                if (ti != FRONT) b.push_back(ti != BACK ? vi.clone() : vi); //Vi is cloned if it was already added to f, clones don't matter for my code because I use objects
                if ((ti | tj) == SPANNING)
                {
                    float t = (w - glm::dot(normal, vi.pos)) / glm::dot(normal, vj.pos - vi.pos);
                    CSGVertex v = vi.interpolate(vj, t);
                    f.push_back(v);
                    b.push_back(v.clone());
                }
            }
            if (f.size() >= 3) front.push_back(CSGPolygon(f));
            if (b.size() >= 3) back.push_back(CSGPolygon(b));
            break;
        }

        default:
        {
            printf("Shouldn't run\n");
            break;
        }
    }
}
