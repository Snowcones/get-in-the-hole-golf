
#include <assert.h>

#ifdef DEBUG

#define GetError()\
{\
    for(GLenum error=glGetError(); error!=GL_NO_ERROR; error=glGetError())\
    {\
        if(error==GL_INVALID_OPERATION)\
        {\
            printf("\n\nGL_INVALID_OPERATION\n\n");\
        }\
        if(error==GL_INVALID_VALUE)\
        {\
            printf("\n\nGL_INVALID_VALUE\n\n");\
        }\
        if(error==GL_INVALID_ENUM)\
        {\
            printf("\n\nGL_INVALID_ENUM\n\n");\
        }\
        if(error==GL_OUT_OF_MEMORY)\
        {\
            printf("\n\nGL_OUT_OF_MEMORY\n\n");\
        }\
        assert(0);\
    }\
}\

#endif

#ifndef DEBUG
#define GetError(){}
#endif