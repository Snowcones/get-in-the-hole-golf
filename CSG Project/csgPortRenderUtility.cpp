//
//  csgPortRenderUtility.cpp
//  CSG Project
//
//  Created by William Henning on 6/15/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include "csgPortRenderUtility.hpp"


void storeVert(float* offset, CSGVertex vert)
{
    offset[0] = vert.pos.x;
    offset[1] = vert.pos.y;
    offset[2] = vert.pos.z;
    offset[3] = vert.normal.x;
    offset[4] = vert.normal.y;
    offset[5] = vert.normal.z;
}

void getTriangleList(CSG tree, int* trisToDraw, float** dataStore)
{
    std::vector<CSGPolygon> polygons = tree.toPolygons();
    
    int totalTris = 0;
    for (int i = 0; i < polygons.size(); i++)
    {
        totalTris += polygons[i].vertices.size() - 2;
    }
    int pointsPerTri = 3;
    int floatsPerPoint = 6;
    *dataStore = (float *)malloc(sizeof(float) * totalTris * pointsPerTri * floatsPerPoint);
    float* data = *dataStore;
    int currPoint = 0;
    for (int p = 0; p < polygons.size(); p++)
    {
        for (int s = 2; s < polygons[p].vertices.size(); s++)
        {
            float* pLoc = &data[currPoint * floatsPerPoint];
            storeVert(pLoc, polygons[p].vertices[0]);
            currPoint++;
            
            
            pLoc = &data[currPoint * floatsPerPoint];
            storeVert(pLoc, polygons[p].vertices[s - 1]);
            currPoint++;
            
            
            pLoc = &data[currPoint * floatsPerPoint];
            storeVert(pLoc, polygons[p].vertices[s]);
            currPoint++;
        }
    }
    *trisToDraw = totalTris;
}

std::vector<triangle> triangleListFromPolygonList(CSGNode tree)
{
    std::vector<CSGPolygon> polygons = tree.allPolygons();
    std::vector<triangle> tris;
    
    for (int p = 0; p < polygons.size(); p++)
    {
        for (int s = 2; s < polygons[p].vertices.size(); s++)
        {
            triangle t = triangle(polygons[p].vertices[0].pos, polygons[p].vertices[s-1].pos, polygons[p].vertices[s].pos);
            t.n = polygons[p].plane.normal; //I think this is right
            tris.push_back(t);
        }
    }
    
    return tris;
}