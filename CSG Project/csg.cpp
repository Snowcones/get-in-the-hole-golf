//
//  csg.cpp
//  CSG Project
//
//  Created by William Henning on 5/5/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

//To Do add support for empty trees
//And with empty tree returns empty tree
//Or with empty tree returns original tree
//Empty tree sub returns empty tree
//Subtracting empty tree does nothing
//Trilist for empty tree returns nothing

//TO DO make csg operations deal with coplanar triangles

#include "csg.hpp"

const float epsilon = 1e-5;
bspTreeNode::bspTreeNode() {
    triNode = triangle();
    frontSubtree = NULL;
    backSubtree  = NULL;
}

const float dEpsilon = 0;

float area(triangle t)
{
    return fabsf(dot(t.a-t.b, t.a-t.c));
}
float dot(const glm::vec3& a, const glm::vec3& b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

void ensureTri(triangle t, bspTreeNode* n)
{
    float fa = n->dist(t.a);
    float fb = n->dist(t.b);
    float fc = n->dist(t.c);
    if(fabsf(fa) < epsilon) { fa = 0; }
    if(fabsf(fb) < epsilon) { fb = 0; }
    if(fabsf(fc) < epsilon) { fc = 0; }
    if (fa * fb < 0 || fa * fc < 0 || fb * fc <0)
    {
        printf("Failed ensure tri");
    }
}

void splitTris(triangle t, triangle splitter, std::vector<triangle>& front, std::vector<triangle>& back, std::vector<triangle>& frontCoplanar, std::vector<triangle>& backCoplanar) {
    
    float fa = splitter.dist(t.a);
    float fb = splitter.dist(t.b);
    float fc = splitter.dist(t.c);

    if(fabsf(fa) < epsilon) { fa = 0; }
    if(fabsf(fb) < epsilon) { fb = 0; }
    if(fabsf(fc) < epsilon) { fc = 0; }
    
    
    if (fa == 0 && fb == 0 && fc == 0)
    {
        //t.n dot splitter.n should be + or - 1 but imprecision makes <0 or >0 a safe check
        if (glm::dot(t.n, splitter.n) >= 0) //Front facing coplanar
        {
            backCoplanar.push_back(t);
            return;
        }
        if (glm::dot(t.n, splitter.n) < 0) //Backwards coplanar
        {
            frontCoplanar.push_back(t);
            return;
        }
    }
    
    if(fa<=0 && fb<=0 && fc<=0)                 //This triangle is completely behind the plane
    {
        back.push_back(t);
    }
    else if (fa>=0 && fb>=0 && fc>=0)           //This triangle is completely in front of the plane
    {
        front.push_back(t);
    }
    else if (fa == 0 || fb == 0 || fc == 0)     //Split tri
    {
        if (fc == 0)
        {
            std::swap(t.c, t.b);
            std::swap(t.a, t.c);
        }
        else if (fa == 0)
        {
            std::swap(t.a, t.b);
            std::swap(t.a, t.c);
        }
        
        glm::vec3 p = splitter.calculateIntersection(t.a, t.c);
        
        triangle t1 = triangle(t.a, p, t.b);
        triangle t2 = triangle(p, t.c, t.b);
        
        if (splitter.dist(t.a) < 0)
        {   //Makes sure t1 is in front, and t2 is behind
            std::swap(t1, t2);
        }
        
        if (glm::dot(t1.n, t.n) < 0)
        {
            //If t is a flipped tri, flip these tris
//            printf("Flipping tris odd split\n");
            t1.n *= -1;
            t1.d *= -1;
            t2.n *= -1;
            t2.d *= -1;
        }
        
        if (glm::dot(t1.n, t.n) < dEpsilon || glm::dot(t2.n, t.n) < dEpsilon)
        {
            printf("What\n");
        }
        
        front.push_back(t1);
        back.push_back(t2);
    }
    else                                        //Straddling tri
    {
        if (fa*fc > 0)
        {
            //printf("AC together\n");
            std::swap(t.b, t.c);
            std::swap(t.a, t.b);
        }
        else if (fa*fb > 0)
        {
            //printf("AB together\n");
        }
        else if (fb*fc > 0)
        {
            //printf("BC together\n");
            std::swap(t.a, t.c);
            std::swap(t.a, t.b);
        }
        else
        {
            //Triangle is straddling bad news man
            printf("Zeros\n");
        }
        
        glm::vec3 A;
        glm::vec3 B;
        
        calculateIntersectionPoints(splitter, t, A, B);
        
        triangle t1 = triangle(t.a, t.b, A);
        triangle t2 = triangle(t.b, B, A);
        triangle t3 = triangle(A, B, t.c);
        
//        float d1 = n->dist(A);
//        float d2 = n->dist(B);
//        
//        ensureTri(t1, n);
//        ensureTri(t2, n);
//        ensureTri(t3, n);
        
        //printf("%f\n", glm::dot(t1.n, t.n));
        if (glm::dot(t1.n, t.n) < dEpsilon)
        {
            //If t is a flipped tri, flip these tris
            //printf("Flipping tris regular split\n");
            t1.n *= -1;
            t1.d *= -1;
            t2.n *= -1;
            t2.d *= -1;
            t3.n *= -1;
            t3.d *= -1;
        }
        
        if (glm::dot(t1.n, t.n) < dEpsilon || glm::dot(t2.n, t.n) < dEpsilon || glm::dot(t3.n, t.n) < dEpsilon) //Had problems with approximate equals
        {
            printf("What\n");
        }
        
        fc = splitter.dist(t.c);
        if (fc>=0)
        {
            back.push_back(t1);
            back.push_back(t2);
            front.push_back(t3);
        }
        else
        {
            front.push_back(t1);
            front.push_back(t2);
            back.push_back(t3);
        }
    }
}

void bspTreeNode::add(triangle t) {
    
    std::vector<triangle> frontTris;
    std::vector<triangle> backTris;
    splitTris(t, this->triNode, frontTris, backTris, frontTris, frontTris);  //Add puts all coplanars in front
    
    if(backTris.size() > 0)             //This triangle is "completely" behind the plane
    {
        for (triangle tri : backTris)   //Add the backTris behind this plane
        {
            if (backSubtree == NULL)
            {
                backSubtree = new bspTreeNode(tri);
            }
            else
            {
                backSubtree->add(tri);
            }
        }
    }
    
    if (frontTris.size() > 0)           //This triangle is "completely" in front of the plane
    {
        for (triangle tri : frontTris)  //Add the frontTris in front of this plane
        {
            if (frontSubtree == NULL)
            {
                frontSubtree = new bspTreeNode(tri);
            }
            else
            {
                frontSubtree->add(tri);
            }
        }
    }
}

bspTreeNode* bspTreeNode::andCSG(bspTreeNode *tree1, bspTreeNode *tree2) {
    return tree1->andCSG(tree2);
}

bspTreeNode* treeFromTriList(const std::vector<triangle>& triList) {
    bspTreeNode* out = new bspTreeNode();
    if (triList.size()>0)
    {
        out->triNode = triList[0];
        for (int i=1; i<triList.size(); i++)
        {
            out->add(triList[i]);
        }
    }
    return out;
}

bspTreeNode* bspTreeNode::andCSG(bspTreeNode *b) { //The boolean function used for all CSG, creates a tree with the geometry common to the given two trees
    bspTreeNode* a = this;
    if (a->isPositiveDegenerate() || b->isPositiveDegenerate())
    {
        printf("andCSG called with a degenerate tree\n");
        return new bspTreeNode();
    }
    else
    {
        std::vector<triangle> insideTriangles;
        
        a->clipTo(b, insideTriangles);
        b->clipTo(a, insideTriangles);
        //a->clipList(insideTriangles);
        
        bspTreeNode* outTree  = treeFromTriList(insideTriangles);
        return outTree;
    }
}

bspTreeNode* bspTreeNode::orCSG(bspTreeNode *tree1, bspTreeNode *tree2) {
    return tree1->orCSG(tree2);
}

bspTreeNode* bspTreeNode::orCSG(bspTreeNode *b) {  // (A | B) =  !( !A & !B)
    bspTreeNode* a = this;
    if (a->isPositiveDegenerate())
    {
        printf("orCSG called with a degenerate tree\n");
        return b;
    }
    else if (b->isPositiveDegenerate())
    {
        printf("orCSG called with a degenerate tree\n");
        return a;
    }
    else
    {
        a->invert();
        b->invert();
        
        bspTreeNode* result = a->andCSG(b);
        
        a->invert();
        b->invert();
        result->invert();
        return result;
    }
}

bspTreeNode* bspTreeNode::subCSG(bspTreeNode *tree1, bspTreeNode *tree2) {
    return tree1->subCSG(tree2);
}

bspTreeNode* bspTreeNode::subCSG(bspTreeNode *b) {  // (A - B) = (A & !B)
    bspTreeNode* a = this;
    if (a->isPositiveDegenerate() || b->isPositiveDegenerate())
    {
        printf("subCSG called with a degenerate tree\n");
        return a;
    }
    else
    {
        b->invert();
        bspTreeNode* result = a->andCSG(b);
        b->invert();
        return result;
    }
}

void bspTreeNode::clipTo(bspTreeNode *clippingTree, std::vector<triangle>& insideTriangles) {
    clippingTree->insert(triNode, insideTriangles);
    if (frontSubtree)
    {
        frontSubtree->clipTo(clippingTree, insideTriangles);
    }
    if (backSubtree)
    {
        backSubtree->clipTo(clippingTree, insideTriangles);
    }
}

void bspTreeNode::clipList(std::vector<triangle>& list) {
    std::vector<triangle> insideTris;
    for (int i=0; i<list.size(); i++)
    {
        insideTris.clear();
        insert(list[i], insideTris);
        
        if (insideTris.size() != 1)           //currTriangle was either split with >=2 pieces or clipped away
        {
            std::swap(list[i], list.back());   //Delete the split/clipped triangle from the inside list
            list.pop_back();
            i--;
            
            for (triangle insideT : insideTris)
            {
                list.push_back(insideT);      //Add the newly split triangles to the inside list
            }
        }
        else
        {
            if (list[i] != insideTris[0]) //currTriangle was split and now insideTris[0] has the split tri
            {
                list[i] = insideTris[0];
            }
        }
    }
}



void bspTreeNode::insert(triangle t, std::vector<triangle>& insideTriangles)
{
    std::vector<triangle> frontTris;
    std::vector<triangle> backTris;
    std::vector<triangle> trash; //??? is this legit
    
//    if (t.A < epsilon*10) //Code for sliver of a triangle
//    {
//        printf("Returning\n");    //I have no idea what this does
//        return;
//    }
    splitTris(t, this->triNode, frontTris, backTris, frontTris, backTris);   //Will clip backwards coplanars
    
    for (triangle tri : frontTris)
    {
        
        if (glm::dot(tri.n, t.n) < dEpsilon)
        {
            printf("Wow\n");
        }
    }
    for (triangle tri : backTris)
    {
        if (glm::dot(tri.n, t.n) < dEpsilon)
        {
            printf("Wow\n");
        }
    }
    
    if (backTris.size() > 0)
    {
        for (triangle tri : backTris)
        {
            if (backSubtree == NULL)
            {
                insideTriangles.push_back(tri);
            }
            else
            {
                backSubtree->insert(tri, insideTriangles);
            }
        }
    }
    
    if (frontTris.size() > 0)
    {
        for (triangle tri : frontTris)
        {
            if (frontSubtree == NULL)
            {
                return;
            }
            else
            {
                frontSubtree->insert(tri, insideTriangles);
            }
        }
    }
}

void bspTreeNode::removeDegenerateFromTree() {
    
}

void bspTreeNode::removeDegenerateFromNode(bspTreeNode *n) {
    
}