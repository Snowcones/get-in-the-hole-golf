//
//  csgPolygon.cpp
//  CSG Project
//
//  Created by William Henning on 7/2/16.
//  Copyright © 2016 William Henning. All rights reserved.
//

#include <stdio.h>
#include <algorithm>

#include "csgPolygon.hpp"
#include "csgVertex.hpp"

//////////////////////////////////
//Original code
//CSGPolygon::CSGPolygon(std::vector<CSGVertex> inVertices):plane(inVertices[0].pos, inVertices[1].pos, inVertices[2].pos) {
//    vertices = inVertices;
//    if (inVertices.size()<3) assert(0);
//}

//My attempt at a bug fix
CSGPolygon::CSGPolygon(std::vector<CSGVertex> inVertices):plane(inVertices) {
    vertices = inVertices;
    if (inVertices.size()<3) assert(0);
}
//////////////////////////////////

CSGPolygon CSGPolygon::clone() {
    return CSGPolygon(vertices);    //Original had more calls to clone
}

void CSGPolygon::flip() {
    for (CSGVertex& v : vertices) {v.flip();}
    std::reverse(vertices.begin(), vertices.end());
    plane.flip();
}
